# material.py: material utilities.
# ------------------------------------------------------
# This file is a part of the KrxImpExp package.
# Author: Vitaly Baranov
# License: GPL
# ------------------------------------------------------
from operator import itemgetter
from pathlib import Path

import bpy
from bpy_extras.node_shader_utils import PrincipledBSDFWrapper
from mathutils import Color
from .preferences import KrxImpExpPreferences

loaded_texture_paths = {}


def load_image(image_name, import_path):
    """Loads image with searching in directories specified by the "paths" global variable"""

    if not image_name:
        return None

    path_exists = False
    if import_path:
        file = Path(import_path).parent.joinpath(image_name)
        path_exists = file.exists()
        file_path = str(file)

    if not path_exists:
        addon_pref: KrxImpExpPreferences = bpy.context.preferences.addons[__package__].preferences
        texture_dirs = addon_pref.texture_directories

        global loaded_texture_paths

        if len(loaded_texture_paths) == 0 and len(texture_dirs) > 0:
            print("Loading texture paths")
            print("Directories:", len(texture_dirs))

            if loaded_texture_paths:
                loaded_texture_paths.clear()

            for texture_dir in reversed(texture_dirs):
                for path in Path(texture_dir.name).glob("*.tga"):
                    loaded_texture_paths[path.name.upper()] = str(path)

            print("Loaded paths:", len(loaded_texture_paths))

        loaded_path = loaded_texture_paths.get(image_name.upper())
        if loaded_path:
            file_path = loaded_path
            path_exists = True

    if path_exists:
        image = bpy.data.images.load(file_path, check_existing=True)
        image.source = "FILE"
        image.filepath = file_path
    else:
        image = bpy.data.images.get(image_name)
        image = image if image else bpy.data.images.new(image_name, 4, 4)
        image.source = "FILE"
        image.filepath = image_name
    return image


def new_material(name):
    """
    Gathers material from bpy.data.materials and returns if it finds similar one by name, else creates new one.
    Counterintuitive to the python's design, but the .get method for bpy.data.materials is faster than finding by try...except
    """
    material_name = name.upper()[:63] # limit of material name, blender has cutting the string right...
    material = bpy.data.materials.get(material_name)
    return material if material else bpy.data.materials.new(material_name)


def image_has_alpha(img):
    b = 32 if img.is_float else 8
    return img.depth == 2 * b or img.depth == 4 * b  # Grayscale+Alpha  # RGB+Alpha


def get_texture_name(material):
    """
    Returns filename string of a texture linked to material.
    Returns an empty string if there is no diffuse map.
    """

    if not material.node_tree:
        return ""

    for node in material.node_tree.nodes:
        if node.type == "TEX_IMAGE":
            if not node.image:
                continue
            return node.image.name
    return ""



def link_texture_to_material(material, image_name, import_file, color_adjustment: float = None):
    """
    Links image texture to material.
    Uses proper node linking to avoid redundant duplicates of images and fake users.
    """
    if color_adjustment is not None and color_adjustment != 0:
        # Check function check_diffuse_color_adjustment() description for more info.
        r, g, b, a = material.diffuse_color
        color = Color((r, g, b))
        material["original_saturation"] = color.s
        # increase HSV Saturation by percentage defined with color_adjustment value
        color.s *= 1 + (color_adjustment * 0.01)
        material.diffuse_color = color.r, color.g, color.b, a
    else:
        material["original_saturation"] = None

    image = load_image(image_name, import_file)
    material.use_backface_culling = True
    material_wrapper = PrincipledBSDFWrapper(material, is_readonly=False)
    material_wrapper.specular = 0
    material.roughness = 1  # blender 4.0
    material.metallic = 1  # blender 4.0
    if image:
        node_tree = material.node_tree
        image_node = node_tree.nodes.new("ShaderNodeTexImage")
        image_node.image = image
        image_node.location = (-500, 0)
        target_link = next(n for n in node_tree.nodes if n.type == "BSDF_PRINCIPLED")
        node_tree.links.new(image_node.outputs["Color"], target_link.inputs["Base Color"])
        if image_has_alpha(image):
            node_tree.links.new(image_node.outputs["Alpha"], target_link.inputs["Alpha"])
            material.show_transparent_back = False
            material.blend_method = "CLIP"
            if hasattr(material, "shadow_method"):
                material.shadow_method = "CLIP"
            material["biplanar"] = True

def check_diffuse_color_adjustment(material: bpy.types.Material) -> tuple:
    """
    So, the thing with material colors imported from files is that, those are dull, it's kind of difficult to
    distinguish between which is which at first glance in viewport. This function brings similar functionality as
    Gothic_MaT_Blender but puts twist on it - this color change will work only in Blender, in viewport.
    When exported, it'll try to restore the original saturation that was imported from file.
    This effect is achieved with Blender's Custom Properties.
    """
    r, g, b, a = material.diffuse_color
    saturation = material.get("original_saturation", None)
    if saturation is None:
        return r, g, b, a

    color = Color((r, g, b))
    color.s = saturation
    return color.r, color.g, color.b, a

class MatLibParser:
    __slots__ = ("__tmaterials", "autonames")

    def __init__(self, filepath, autonaming=False):
        self.__tmaterials = list()
        self.autonames = autonaming
        self.load_material_filter(filepath)

    @property
    def materials(self) -> list:
        if self.__tmaterials:
            if len(self.__tmaterials) > 0:
                return self.__tmaterials
        return None

    def name(self, index) -> dict:
        return self.materials[index]["name"]

    def texture(self, index) -> dict:
        return self.materials[index]["texture"]

    def material(self, index) -> dict:
        return self.materials[index]

    def set_material(self, index, value):
        if self.__tmaterials:
            if len(self.__tmaterials) > 0:
                self.__tmaterials[index] = value

    def __pml_parse(self, pml_file_path):
        try:
            with open(pml_file_path, "rt", encoding="Windows-1250") as file_handle:
                for line in file_handle:
                    if "[% zCMaterial" in line:
                        tmaterial = dict()
                    data = line.rstrip("\r\n").lstrip("\t").split("=")
                    if len(data) == 2:
                        key = data[0]
                        value_type = data[1].split(":")[0]
                        value = data[1].split(":")[1]
                        if value_type.find("enum") != -1 or value_type.find("int") != -1:
                            value = int(value)
                        elif value_type.find("float") != -1:
                            value = float(value)
                        elif value_type.find("bool") != -1:
                            value = bool(value)
                        elif value_type.find("string") != -1:
                            value = str(value)
                        elif value_type.find("color") != -1:
                            value = value.split()
                            value = [int(val) for val in value]
                        elif value_type.find("rawFloat") != -1:
                            value = value.split()
                            value = [float(val) for val in value]
                        tmaterial[key] = value

                    if line.find("[]") != -1:
                        self.__tmaterials.append(tmaterial)
        # EnvironmentError = parent of IOError, OSError *and* WindowsError where available
        except (EnvironmentError, AttributeError, ValueError) as e:
            self.__tmaterials = list()
            self.autonames = False

    def load_material_filter(self, mat_lib_ini_path: str):
        if mat_lib_ini_path:
            if len(self.__tmaterials) > 0:
                self.__tmaterials = list()
                self.autonames = False
            try:
                with open(mat_lib_ini_path, "rt", encoding="Windows-1250") as file_handle:
                    self.__tmaterials = []
                    for line in file_handle:
                        line = line.rstrip("\r\n")
                        pos = line.find("=")
                        if pos != -1:
                            pml_file_path = Path(mat_lib_ini_path).parent / f"{line[:pos]}.pml"
                            self.__pml_parse(pml_file_path)

                    self.__tmaterials.sort(key=itemgetter("name"))
            # parent of IOError, OSError *and* WindowsError where available
            except (EnvironmentError, AttributeError) as e:
                self.__tmaterials = list()
                self.autonames = False
