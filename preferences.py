"""
preferences.py: provides all of the functions and classes related to user add-on preferences.
This file also provides GUI for each preference toggle. (it was formely in image_search.py)
-------------------------------------------------------------------------------------------------------
This file is a part of the KrxImpExp package.
Author: Vitaly Baranov
License: GPL
-------------------------------------------------------------------------------------------------------
"""
import os
import typing

import bpy

from . import system


class TextureDirectory(bpy.types.PropertyGroup):
    """
    Subclass of the PropertyGroup used for the CollectionProperty type in the preferences class.
    Represents a texture directory item with the path
    """

    name: str
    """Unique name of the item and the directory path itself"""


class KrxImpExpPreferences(bpy.types.AddonPreferences):
    """AddonPreferences class of the KrxImpExp add-on"""

    bl_idname: str = __package__

    create_log_file: bpy.props.BoolProperty(
        name="Create Log File",
        description="Toggle creating log file with verbose debug data.\nMost operations won't output debug data",
        default=True,
    )

    rescan_textures_every_time: bpy.props.BoolProperty(
        name="Search textures on every import",
        description="Toggle re-scanning texture directories on every import instead of only the first import.\n"
        "Useful when you add / remove texture files while Blender is still running",
        default=True,
    )

    texture_directories: bpy.props.CollectionProperty(
        name="Texture directories",
        description="Collection of texture directories",
        type=TextureDirectory,
    )

    selected_texture_directory: bpy.props.IntProperty(
        name="Selected texture directory",
        description="Index of the selected directory",
        default=-1,
        min=-1,
    )

    def draw(self, context: bpy.types.Context):
        KrxImpExpPreferencesManager.draw_preferences_panel(slf=self, context=context)


class KrxImpExpPreferencesManager:
    """
    Class which manages the KrxImpExp add-on preferences.
    There should be no instance of this class.
    The class properties are synced with the KrxImpExpPreferences data on each drawn frame.
    """

    def __init__(self):
        raise NotImplementedError(self.__doc__)

    addon_pref: KrxImpExpPreferences
    texture_dirs: bpy.props.CollectionProperty
    selected_index: bpy.props.IntProperty

    dependencies_installed: bool
    """The flag is set externally in the __init__ package file"""

    error_message: str
    """The error message is set externally in the __init__ package file"""

    @classmethod
    def write_log(cls, message: str, mode: str = "a") -> None:
        preferences = bpy.context.preferences.addons[__package__].preferences

        if not preferences.create_log_file:
            return

        system.write_log(message, mode)

    @classmethod
    def add_path(cls, *, path: str) -> None:
        """Add a path to the end of the list"""

        if not path:
            return

        path = os.path.join(path, "")

        if path not in cls.texture_dirs:
            item = cls.texture_dirs.add()
            item.name = path
            cls.addon_pref.selected_texture_directory = len(cls.texture_dirs) - 1

    @classmethod
    def add_path_with_subpaths(cls, *, path: str):
        """Add multiple paths to the end of the list"""

        if not path:
            return

        for root, _, __ in os.walk(path, followlinks=True):
            cls.add_path(path=root)

    @classmethod
    def move_path(cls, *, direction: int) -> None:
        """Move the currently selected path in a certain direction"""

        new_index = cls.selected_index + direction

        if 0 <= new_index < len(cls.texture_dirs):
            cls.texture_dirs.move(cls.selected_index, new_index)
            cls.addon_pref.selected_texture_directory = new_index

    @classmethod
    def remove_path(cls) -> None:
        """Remove the currently selected path"""

        if not cls.texture_dirs or cls.selected_index not in range(len(cls.texture_dirs)):
            return

        cls.texture_dirs.remove(cls.selected_index)

        if cls.selected_index >= len(cls.texture_dirs):
            cls.addon_pref.selected_texture_directory = len(cls.texture_dirs) - 1

    @classmethod
    def remove_all_paths(cls) -> None:
        """Remove all paths"""

        while cls.texture_dirs:
            cls.texture_dirs.remove(0)

        cls.addon_pref.selected_texture_directory = -1

    @classmethod
    def draw_preferences_panel(cls, *, slf: bpy.types.Panel, context: bpy.types.Context) -> None:
        layout: bpy.types.UILayout = slf.layout

        if cls.error_message:
            layout.label(text=f"{cls.error_message} - Check Console", icon="ERROR")
            layout.operator("wm.console_toggle", icon="CONSOLE")
            return

        if not cls.dependencies_installed:
            layout.label(text="Requires installation of dependencies", icon="INFO")
            layout.operator("krxpref.install_dependencies", icon="CONSOLE")
            return

        # sync the changes from the UIList with data in the Manager class
        preferences: bpy.types.Preferences = context.preferences
        cls.addon_pref = preferences.addons[__package__].preferences
        cls.texture_dirs = cls.addon_pref.texture_directories
        cls.selected_index = cls.addon_pref.selected_texture_directory

        layout.operator(operator="krxpref.open_plugin_directory", text="Open Plugin Directory", icon="WINDOW")
        layout.prop(data=cls.addon_pref, property="create_log_file")
        layout.prop(data=cls.addon_pref, property="rescan_textures_every_time")

        layout.label(text="List of texture directories (used by the KrxImpExp scripts)", icon="OUTLINER")

        row_textures_content: bpy.types.UILayout = layout.row()
        row_textures_content.template_list(
            listtype_name="KRXPREF_UL_list_slot",
            list_id="",
            dataptr=cls.addon_pref,
            propname="texture_directories",
            active_dataptr=cls.addon_pref,
            active_propname="selected_texture_directory",
            rows=5,
            maxrows=5,
        )
        col_textures_right: bpy.types.UILayout = row_textures_content.column(align=True)
        col_textures_right.scale_x = 0.75
        col_textures_right.operator(operator="krxpref.add_path", text="Add path", icon="ADD")
        col_textures_right.operator(
            operator="krxpref.add_path_with_subpaths", text="Add path with subpaths", icon="PLUS"
        )
        col_textures_right.operator(operator="krxpref.move_path", text="Move path up", icon="TRIA_UP").direction = -1
        col_textures_right.operator(operator="krxpref.move_path", text="Move path down", icon="TRIA_DOWN").direction = 1
        col_textures_right.operator(operator="krxpref.remove_path", text="Remove path", icon="REMOVE")
        col_textures_right.operator(operator="krxpref.remove_all_paths", text="Remove all paths", icon="X")

        total: str = f"Total: {len(cls.texture_dirs)}"
        if cls.selected_index in range(len(cls.texture_dirs)):
            total += f" | Selected: {cls.selected_index + 1} - {cls.texture_dirs[cls.selected_index].name}"

        layout.label(text=total)

        if context.scene.get("texture_directories") is not None:
            layout.label(text="The current scene contains a deprecated directory list:", icon="ERROR")
            layout.operator(operator="krxpref.remove_legacy", text="Remove old paths from all scenes", icon="X")


class KRXPREF_OT_install_dependencies(bpy.types.Operator):
    """Installs 'dearpygui', an external graphics library for this add-on."""

    bl_idname = "krxpref.install_dependencies"
    bl_label = "Install KrxImpExp's Dependencies"
    bl_options = {"REGISTER", "INTERNAL"}

    def execute(self, context):
        from . import DEPENDENCIES, import_module, register_krximpexp_internals

        try:
            for dependency in DEPENDENCIES:
                import_module(dependency, attempt_install=True)
        except (ModuleNotFoundError, ImportError, RuntimeError) as err:
            KrxImpExpPreferencesManager.error_message = "Installation Error"
            print("Installation Error\n", err)
            self.report({"ERROR"}, str(err))
            return {"CANCELLED"}

        KrxImpExpPreferencesManager.dependencies_installed = True

        # Register the panels, operators, etc. since dependencies are installed
        register_krximpexp_internals()

        return {"FINISHED"}


class KRXPREF_PT_panel(bpy.types.Panel):
    """Panel to hold all of the KrxImpExp preferences"""

    bl_space_type: str = "PREFERENCES"
    bl_region_type: str = "WINDOW"
    bl_label: str = "KrxImpExp Add-on Preferences"

    def draw(self, context: bpy.types.Context):
        KrxImpExpPreferencesManager.draw_preferences_panel(slf=self, context=context)


class KRXPREF_OT_open_plugin_directory(bpy.types.Operator):
    """Opens plugin root directory, which could contain logs"""

    bl_idname = "krxpref.open_plugin_directory"
    bl_label = "Open Plugin Directory"
    bl_options = {"REGISTER", "INTERNAL"}

    def execute(self, context):
        system.open_plugin_directory()
        return {"FINISHED"}


class KRXPREF_UL_list_slot(bpy.types.UIList):
    """List of texture directories"""

    def draw_item(
        self,
        context: bpy.types.Context,
        layout: bpy.types.UILayout,
        data: bpy.types.AnyType,
        item: TextureDirectory,
        icon: int,
        active_data: bpy.types.AnyType,
        active_property: str,
        index: int = 0,
        flt_flag: int = 0,
    ):
        layout.label(text=item.name)


class KRXPREF_OT_add_path(bpy.types.Operator):
    """Add a path to the list"""

    bl_idname: str = "krxpref.add_path"
    bl_label: str = "Add path"

    directory: bpy.props.StringProperty(name="Directory Path")
    """This property is used by the file browser in `invoke`"""

    def invoke(self, context: bpy.types.Context, _: bpy.types.Event) -> typing.Union[typing.Set[str], typing.Set[int]]:
        context.window_manager.fileselect_add(self)
        return {"RUNNING_MODAL"}

    def execute(self, _: bpy.types.Context) -> typing.Union[typing.Set[str], typing.Set[int]]:
        KrxImpExpPreferencesManager.add_path(path=self.directory)
        bpy.ops.wm.save_userpref()
        return {"FINISHED"}


class KRXPREF_OT_add_path_with_subpaths(bpy.types.Operator):
    """Add a path with subpaths to the list"""

    bl_idname: str = "krxpref.add_path_with_subpaths"
    bl_label: str = "Add path with subpaths"

    directory: bpy.props.StringProperty(name="Directory Path")
    """This property is used by the file browser in `invoke`"""

    def invoke(self, context: bpy.types.Context, _: bpy.types.Event) -> typing.Union[typing.Set[str], typing.Set[int]]:
        context.window_manager.fileselect_add(self)
        return {"RUNNING_MODAL"}

    def execute(self, _: bpy.types.Context) -> typing.Union[typing.Set[str], typing.Set[int]]:
        KrxImpExpPreferencesManager.add_path_with_subpaths(path=self.directory)
        bpy.ops.wm.save_userpref()
        return {"FINISHED"}


class KRXPREF_OT_move_path(bpy.types.Operator):
    """Change order of the list items"""

    bl_idname: str = "krxpref.move_path"
    bl_label: str = "Move path"

    direction: bpy.props.IntProperty(name="Index offset", default=0, min=-1, max=1)

    def invoke(self, _: bpy.types.Context, __: bpy.types.Event) -> typing.Union[typing.Set[str], typing.Set[int]]:
        KrxImpExpPreferencesManager.move_path(direction=self.direction)
        bpy.ops.wm.save_userpref()
        return {"FINISHED"}


class KRXPREF_OT_remove_path(bpy.types.Operator):
    """Remove a path from the list"""

    bl_idname: str = "krxpref.remove_path"
    bl_label: str = "Remove path"

    def invoke(self, _: bpy.types.Context, __: bpy.types.Event) -> typing.Union[typing.Set[str], typing.Set[int]]:
        KrxImpExpPreferencesManager.remove_path()
        bpy.ops.wm.save_userpref()
        return {"FINISHED"}


class KRXPREF_OT_remove_all_paths(bpy.types.Operator):
    """Remove all paths from the list"""

    bl_idname: str = "krxpref.remove_all_paths"
    bl_label: str = "Remove all paths"

    def invoke(self, _: bpy.types.Context, __: bpy.types.Event) -> typing.Union[typing.Set[str], typing.Set[int]]:
        KrxImpExpPreferencesManager.remove_all_paths()
        bpy.ops.wm.save_userpref()
        return {"FINISHED"}


class KRXPREF_OT_remove_legacy(bpy.types.Operator):
    """Remove legacy `texture_directories` from all scenes"""

    bl_idname: str = "krxpref.remove_legacy"
    bl_label: str = "Remove old paths from all scenes"

    def invoke(self, _: bpy.types.Context, __: bpy.types.Event) -> typing.Union[typing.Set[str], typing.Set[int]]:
        for scene in bpy.data.scenes:
            if scene.get("texture_directories") is not None:
                del scene["texture_directories"]
            if scene.get("selected_texture_directory") is not None:
                del scene["selected_texture_directory"]

        return {"FINISHED"}


def register():
    KrxImpExpPreferencesManager.dependencies_installed = False
    KrxImpExpPreferencesManager.error_message = ""

    bpy.utils.register_class(TextureDirectory)
    bpy.utils.register_class(KrxImpExpPreferences)

    bpy.utils.register_class(KRXPREF_OT_install_dependencies)
    bpy.utils.register_class(KRXPREF_OT_open_plugin_directory)

    bpy.utils.register_class(KRXPREF_PT_panel)
    bpy.utils.register_class(KRXPREF_UL_list_slot)
    bpy.utils.register_class(KRXPREF_OT_add_path)
    bpy.utils.register_class(KRXPREF_OT_add_path_with_subpaths)
    bpy.utils.register_class(KRXPREF_OT_move_path)
    bpy.utils.register_class(KRXPREF_OT_remove_path)
    bpy.utils.register_class(KRXPREF_OT_remove_all_paths)
    bpy.utils.register_class(KRXPREF_OT_remove_legacy)


def unregister():
    bpy.utils.unregister_class(KrxImpExpPreferences)
    bpy.utils.unregister_class(TextureDirectory)

    bpy.utils.unregister_class(KRXPREF_OT_open_plugin_directory)
    bpy.utils.unregister_class(KRXPREF_OT_install_dependencies)

    bpy.utils.unregister_class(KRXPREF_OT_remove_legacy)
    bpy.utils.unregister_class(KRXPREF_OT_remove_all_paths)
    bpy.utils.unregister_class(KRXPREF_OT_remove_path)
    bpy.utils.unregister_class(KRXPREF_OT_move_path)
    bpy.utils.unregister_class(KRXPREF_OT_add_path_with_subpaths)
    bpy.utils.unregister_class(KRXPREF_OT_add_path)
    bpy.utils.unregister_class(KRXPREF_UL_list_slot)
    bpy.utils.unregister_class(KRXPREF_PT_panel)
