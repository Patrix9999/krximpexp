# KrxImpExp for Blender ⚙️

[Kerrax Import Export](http://krximpexp.sourceforge.net) was an open source collection of extensions made for 3D editors that added support for 3D models from the [Gothic 1](https://store.steampowered.com/app/65540/Gothic_1/) and [Gothic 2](https://store.steampowered.com/app/39510/Gothic_II_Gold_Edition) game series.

This project's goal is to make this awesome add-on compatible with newer [Blender](https://www.blender.org) versions with an API level of [2.80+](https://wiki.blender.org/wiki/Reference/Release_Notes/2.80/Python_API), and increase its feature set in the future. See supported versions [below](#supported-versions-).

Thanks to the new UI, this project now supports Windows, Linux and macOS, instead of only Windows like before.

This project is supported by the Gothic Modding Community.  
To chat about this project join the [GMC Discord server](https://discord.gg/wbsVaE9mCn).

> The new modern import and export UI was built using the [DearPy GUI](https://github.com/hoffstadt/DearPyGui) module. It is a dependency that will be installed after enabling the add-on.   
> Based on the downloaded ZIP archive it might require network access during the installation process.

[![join-community](https://img.shields.io/badge/join-community-5865f2?logo=Discord&style=for-the-badge)](https://discord.gg/wbsVaE9mCn)

## Project Features 📝

- [X] **.3DS** import/export
- [X] **.ASC** import/export
- [X] **.MRM** import
- [X] **.MSH** import
- [X] **.ZEN** import

## Installation 📥

This project supports all 3 possible methods of installing Blender add-ons.

### Preparation

Firstly, choose a ZIP archive containing the source files:

1. Experimental `dev` branch code.

   [![download](https://img.shields.io/badge/download-dev-orange?logo=Blender&style=for-the-badge)](https://gitlab.com/Patrix9999/krximpexp/-/archive/dev/krximpexp-dev.zip)

> Currently we only endorse and recommend the usage of the `dev` branch.

<details>
<summary>Older versions</summary>

2. ~~Stable `main` branch code.~~

   [![download](https://img.shields.io/badge/download-main-blue?logo=Blender&style=for-the-badge)](https://gitlab.com/Patrix9999/krximpexp/-/archive/main/krximpexp-main.zip)  
3. ~~The latest stable release.~~

   [![release](https://img.shields.io/badge/latest-release-brightgreen?logo=GitLab&style=for-the-badge)](https://gitlab.com/Patrix9999/krximpexp/-/releases/permalink/latest)  

</details>


### Method 1 (Recommended) - Install add-on inside your custom scripts directory)

1. Create a custom `scripts` directory, somewhere in your file system. For example in your `Documents` or `~` directory etc.
2. Create an `addons` directory inside the custom `scripts` directory.
3. Extract the downloaded archive into the `addons` directory.
4. Open up Blender and add the `scripts` directory to your preferences:
   - Open `Edit` -> `Preferences` -> `File Paths`
   - Select the folder icon next to `Data` -> `Scripts`
   - Select the `scripts` directory created in step 1.
   - Open up the burger menu and select `Save Preferences`
   - Close Blender down
5. Open up Blender and enable the add-on:
   - Open `Edit` -> `Preferences` -> `Add-ons`
   - Search for `KrxImpExp`
   - Enable the add-on by clicking on the checkbox
6. Install dependencies:
   - Expand the add-on display
   - Press the `Install KrxImpExp` button
7. Enjoy

### Method 2 - Install add-on inside the Blender installation directory

<details>

> Keep in mind that the plugin **_requires_** write access inside its own directory. Therefore, it is recommended to use a Blender installation outside the `Program Files` directory when using this method.

> If you're using Linux with [Flatpak](https://flatpak.org/) or some other sandboxed package manager, use another method, since likely there is no write access in the installation directory. 

1. Navigate to your Blender installation `addons` directory. Examples:
   - `C:\Program Files\Blender Foundation\Blender 3.3\3.3\scripts\addons`
   - `C:\Users\GothicEnjoyer\AppData\Roaming\Blender Foundation\Blender\3.3\scripts\addons`
   - `C:\Program Files (x86)\Steam\steamapps\common\Blender\3.3\scripts\addons`
   - `/usr/lib/blender/scripts/addons`
2. Extract the downloaded archive into the `addons` directory.
3. Open up Blender and enable the add-on:
   - Open `Edit` -> `Preferences` -> `Add-ons`
   - Search for `KrxImpExp`
   - Enable the add-on by clicking on the checkbox
4. Install dependencies:
   - Expand the add-on display
   - Press the `Install KrxImpExp` button
5. Enjoy

</details>

### Method 3 - Install using the automatic ZIP installation

<details>

1. Open up Blender and install and enable the add-on:
   - Open `Edit` -> `Preferences` -> `Add-ons`
   - Select the `Install...` option above
   - Select the downloaded archive
   - Enable the add-on by clicking on the checkbox
2. Install dependencies:
   - Expand the add-on display
   - Press the `Install KrxImpExp` button
3. Enjoy

</details>

## Issue Reporting 📌

If you find any issues with this project, report them in [project issues](https://gitlab.com/Patrix9999/krximpexp/-/issues).
Add the description of the issue, steps to reproduce it, and a minimal example file that triggers the issue.

You can also join the [GMC Discord server](https://discord.gg/wbsVaE9mCn) and as there if you prefer this approach.

[![join-community](https://img.shields.io/badge/join-community-5865f2?logo=Discord&style=for-the-badge)](https://discord.gg/wbsVaE9mCn)

## Contributing 🤝

Any contribution to this repository is welcome.  
To add your own changes to this project, just open a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

> To get auto-complete support in your IDE install the [fake-bpy-module](https://github.com/nutti/fake-bpy-module/) and if possible add `Blender/scripts/modules` to your Python `PATH`.

## Supported Versions 📈

This KrxImpExp edition supports the following Blender versions:

<details open>
<summary>If there is a newer version available and not listed here, please try it out and provide feedback</summary>

- Blender 2.80 (64-bit only)
- Blender 2.81
- Blender 2.82
- Blender 2.83
- Blender 2.90
- Blender 2.91
- Blender 2.92
- Blender 2.93
- Blender 3.0
- Blender 3.1
- Blender 3.2
- Blender 3.3
- Blender 3.4
- Blender 3.5
- Blender 3.6
- Blender 4.0

</details>