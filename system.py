import datetime
import json
import os
import platform
import subprocess
import sys
from pathlib import Path
from typing import Dict, Optional

MACHINE: str = platform.machine().upper()
"""Machine/CPU architecture"""

PYTHON_VERSION: str = platform.python_version()
"""Version of the running Python executable"""

PYTHON_BIN: str = sys.executable
"""String path to Blender's Python executable"""

SYSTEM: str = platform.system().upper()
"""System variant"""

PLUGIN_ROOT: Path = Path(os.path.dirname(os.path.realpath(__file__)))
"""Path to the root directory of the plugin"""

PLUGIN_VENV: Path = PLUGIN_ROOT / "venv"
"""Path for the installation of dependencies"""

PLUGIN_VENV_BIN: Path = PLUGIN_VENV / ("Scripts" if SYSTEM == "WINDOWS" else "bin") / ("python.exe" if SYSTEM == "WINDOWS" else "python")
"""System aware path to the activation script for the venv"""

PLUGIN_SITE_PACKAGES: str = str(PLUGIN_ROOT / "gui" / "site-packages")
"""String path to the site-packages directory of the plugin. Used by pip to install packages."""

LOG_FILE_NAME: str = f"{datetime.datetime.now().strftime('%Y%m%d%H%M%S')}_KRX.log"
"""Filename of the current log for this Blender run time"""

OBFUSCATION_MAP = {
    str(PLUGIN_ROOT): f"...{os.sep}PLUGIN_ROOT",
    str(PYTHON_BIN): f"...{os.sep}PYTHON_BIN",
    os.getlogin(): f"USER-LOGIN"
}
"""When logging to file hide these keys with values"""


def is_write_access_available() -> bool:
    packages_dir: Path = Path(PLUGIN_SITE_PACKAGES)
    packages_file: Path = packages_dir / "KrxImpExp-WriteTest.log"

    try:
        # Test write access
        packages_dir.mkdir(parents=True, exist_ok=True)
        packages_file.write_text("Test", encoding="utf8")
        packages_file.unlink()
        return True
    except (PermissionError, OSError) as err:
        print(err)
        return False


def is_venv_available() -> bool:
    return PLUGIN_VENV_BIN.exists() and PLUGIN_VENV_BIN.is_file()


def create_venv() -> None:
    output = call_python_process("-m", "venv", str(PLUGIN_VENV))
    if output:
        print(output)


def local_dearpygui_package_name(package_version: str, with_extension: bool = True) -> str:
    """
    Returns package name based on the running OS
    Example With Extension:    dearpygui-1.6.2-cp310-cp310-win_amd64.whl
    Example Without Extension: dearpygui-1.6.2-cp310-cp310-win_amd64
    """

    minimum_version = (1, 6, 2)
    compare_version = tuple(map(int, package_version.split(".")))

    for c, m in zip(compare_version, minimum_version):
        if c < m:
            raise Exception(f"KrxImpExp: '{package_version}' dearpygui version not supported")
        if c > m:
            break

    python_mapping: Dict[str, str] = {
        "3.7": "cp37-cp37m",
        "3.8": "cp38-cp38",
        "3.9": "cp39-cp39",
        "3.10": "cp310-cp310",
        "3.11": "cp311-cp311",
        "3.12": "cp312-cp312",
        "3.13": "cp313-cp313",
    }

    for version in python_mapping:
        if version in PYTHON_VERSION:
            py_ver = python_mapping[version]
            break
    else:
        raise Exception(f"KrxImpExp: '{PYTHON_VERSION}' Python version not supported")

    sys_ver: str = ""
    cpu_ver: str = ""

    if SYSTEM == "WINDOWS":
        sys_ver = "win"
        if "AMD64" in MACHINE:
            cpu_ver = "amd64"
    elif SYSTEM == "LINUX":
        sys_ver = "manylinux1"
        if "X86_64" in MACHINE:
            cpu_ver = "x86_64"
    elif SYSTEM == "DARWIN":
        print(f"Warning: MacOS wasn't tested! Something might be broken!")
        sys_ver = "macosx"
        if "X86_64" in MACHINE:
            cpu_ver = "10_6_x86_64"
        elif "ARM" in MACHINE:
            cpu_ver = "11_0_arm64"

    if not sys_ver or not cpu_ver:
        raise Exception(f"KrxImpExp: '{SYSTEM}' system or '{MACHINE}' cpu version not supported")

    return f"dearpygui-{package_version}-{py_ver}-{sys_ver}_{cpu_ver}{'.whl' if with_extension else ''}"


def call_python_process(*args) -> Optional[str]:
    """Spawns Python process with arguments"""

    python_bin = str(PLUGIN_VENV_BIN) if is_venv_available() else PYTHON_BIN
    args = args[:-1] if is_venv_available() and args[-1] == "--user" else args
    args = (python_bin, *args)
    command = ' '.join(args)

    print(f"KrxImpExp: running command {command}")
    
    with subprocess.Popen(args=args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False) as process:
        output, errors = process.communicate()
        output = output.decode("utf8", "replace").strip() or "Output is empty\n"
        errors = errors.decode("utf8", "replace").strip() or "No errors found\n"
        if output or errors:
            result = f"Result for {command}"
            result += f"\nOutput:\n{output}"
            result += f"\nErrors:\n{errors}"
            return result
    
    return None


def install_dependency(dependency: str, version: str, fallback: str = None) -> bool:
    """Installs a given dependency.
    Calls the internal Blender Python executable to install dependency into local site-packages folder,
    so that we can use for example dearpygui.
    """

    package_path: Path = PLUGIN_ROOT / "packages" / local_dearpygui_package_name(version)
    install_target: str = str(package_path)

    if not package_path.exists():
        print(f"'{package_path}' does not exist, trying to install via network...")
        install_target = f"{dependency}=={version}"

    output = ""
    
    try:
        output += call_python_process("-m", "ensurepip", "--user")
        output += call_python_process("-m", "pip", "install", f"--target={PLUGIN_SITE_PACKAGES}", install_target)
    except (subprocess.SubprocessError, subprocess.CalledProcessError) as err:
        pass
    else:
        err = None

    if version is None:
        validation_token = f"Successfully installed {dependency}"
    else:
        validation_token = f"Successfully installed {dependency}-{version}"

    print(f"Installation log:\n{output}")

    if err:
        raise RuntimeError(str(err))

    if output and validation_token in output:
        return True
    else:
        if fallback:
            return install_dependency(dependency, fallback)
        print(f"Did not find the validation_token: '{validation_token}'")
        return False


class JsonHandler:
    temp_path: Path = PLUGIN_ROOT / "gui" / "_temp"

    @classmethod
    def load(cls, tool: str, dir_path: Path = None) -> Optional[Dict]:
        if dir_path is None:
            dir_path = cls.temp_path

        file_handle: Path = dir_path / f"{tool}.json"

        if not file_handle.exists():
            return None

        with file_handle.open(encoding="utf8") as file:
            return json.load(file)

    @classmethod
    def save(cls, obj, tool: str, dir_path: Path = None) -> None:
        if dir_path is None:
            dir_path = cls.temp_path

        dir_path.mkdir(parents=True, exist_ok=True)
        file_handle: Path = dir_path / f"{tool}.json"

        with file_handle.open(mode="w", encoding="utf8") as file:
            json.dump(obj, fp=file, default=vars, indent=4, sort_keys=True, ensure_ascii=False)


def load_temp_json_file(tool: str) -> Optional[Dict]:
    return JsonHandler.load(tool)


def save_temp_json_file(val, tool: str) -> None:
    JsonHandler.save(obj=val, tool=tool)


def format_json_for_logging(data: dict) -> str:
    # Do not modify the reference, make a copy
    local = {**data}

    # Obfuscate paths
    for key, value in local.items():
        if "path" in key.lower() or "file" in key.lower() and isinstance(value, str):
            safe_to_show_parent = Path(value).parent.parent.parent.parent.parent
            relative_path = str(Path(value).relative_to(safe_to_show_parent))
            local[key] = "..." + relative_path

    return json.dumps(local, default=vars, indent=2, sort_keys=True, ensure_ascii=False)


def write_log(content: str, mode: str = "a", filename: str = LOG_FILE_NAME):
    if content:
        for key, value in OBFUSCATION_MAP.items():
            content = content.replace(key, value)

    with open(PLUGIN_ROOT / filename, mode) as file:
        file.write(content)
        if mode != "w":
            file.write("\n#---#\n")


def prune_logs(root: Path = PLUGIN_ROOT):
    logs = []
    for path in root.iterdir():
        if path.suffix.lower() == ".log":
            logs.append(path)

    # Descending order based on name
    logs = sorted(logs, key=lambda p: p.name, reverse=True)

    while len(logs) > 7:
        path = logs.pop()
        print(f"Removing {path.name}")
        path.unlink()

def open_plugin_directory():
    if SYSTEM == "WINDOWS":
        args = ["explorer"]
    elif SYSTEM == "LINUX":
        args = ["xdg-open"]  # TODO Linux not tested
    elif SYSTEM == "DARWIN":
        args = ["open"]  # TODO macOS not tested

    args.append(str(PLUGIN_ROOT))
    subprocess.Popen(args, shell=True)
