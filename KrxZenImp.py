from .gui import call_gui, call_message_box
from .KrxMshImp import zenginWorldLoader
from .scene import reset_scene
from .system import load_temp_json_file
from .helpers import CallUIType


# Only for internal use by KrxImportExportManager function
def KrxZenImpGUI(filename: str):
    ui_messages = call_gui(filename, CallUIType.IMPORT_ZEN)

    if not ui_messages:
        return None
    elif "RESTART_GUI" in ui_messages:
        KrxZenImpGUI(filename)
        return None
    elif "SUCCESS" in ui_messages:
        ui_data_result = load_temp_json_file(tool="OUTPUT_IMPORT")

        if not ui_data_result:
            call_message_box(error_message="Code -1: Invalid JSON data!")
            return None

        if ui_data_result["scene_mode"] == 1:
            reset_scene()

        zenginWorldLoader().ReadZENFile(
            filename,
            ui_data_result["scale"],
            ui_data_result["remove_sectored_materials"],
            ui_data_result["color_adjustment"],
        )


# For calling outside KrxImpExp module
def KrxZenImp(
    filename: str,
    scale: float = 0.01,
    remove_sectored_materials: bool = True,
    color_adjustment: float = None,
):
    zenginWorldLoader().ReadZENFile(
        filename,
        scale,
        remove_sectored_materials=remove_sectored_materials,
        color_adjustment=color_adjustment,
    )
