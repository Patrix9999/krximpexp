# Why it's done the way it is? Because generating pot file from one file is 100x better than having it skim through the project, which is just painful
# Generating translation template is streightforward, simply execute this command: 
# >python "Python311\Tools\i18n\pygettext.py" -d base -o base_translation.pot .\gui\gui_values.py
# This command will spit out .pot file that you can open in some webbased solution or poedit
# PLEASE DO NOT FORMAT THIS FILE WITH BLACK MODULE EVER! The way this file is structured is strictly tied to how gettext insert's strings!

from gui_system import LanguageManager

from helpers import ModelType

language_handler = LanguageManager()
_ = language_handler.gettext_handle.gettext

WEBSITE = "https://gitlab.com/Patrix9999/krximpexp/"

scale_units = [
    _("inch"),
    _("foot"),
    _("mile"),
    _("millimeter"),
    _("centimeter"),
    _("meter"),
    _("kilometer")
]

scale_units_value = (
    1 / 2.54,
    1 / 30.48,
    1 / 160934.4,
    1 / 0.1,
    1,
    1 / 100, # Meter, Default
    1 / 100000
)

default_scale = 5 # Meter, Default
default_scale_value_import = scale_units_value[default_scale]
default_scale_value_export = 1 / default_scale_value_import

# Strings
# Friendly Window titles, non-translatable due to wchar bug with DPG https://github.com/BramblePie/imgui/commit/b322cc0f5d7f54f339450f804c2b484c4c4b2789

window_title_export_3ds = "3D Studio Mesh Exporter"
window_title_export_asc = "ASCII Model Exporter"
window_title_import_asc_dynamic_ani = "ASCII Dynamic Animation Importer"
window_title_import_asc_model = "ASCII Skeletal Model Importer"
window_title_import_asc_morphmesh_ani = "ASCII MorphMesh Animation Importer"
window_title_import_asc_morphmesh = "ASCII MorphMesh Model Importer"
window_title_import_mesh = "Static Mesh Importer"

language_text = _("Language")
interface_size_text = _("Interface scale")

# file
file_properties_text = _("File Properties")
file_size_text = _("File size")
file_name_text = _("File name")
file_path_text = _("File path")


# Scene
scene_todo_text = _("What to do with current scene")
anim_todo_text = _("What to do with current animation")
scene_todo_existing_model_text = _("What to do with the existing model")

coord_system_desc = [
    _("Use this item to save levels. You won't need to attach all objects to a single mesh before exporting."),
    _("Use this item to save items and MOBs. The vertices of any objects will be saved relative to the object's pivot."),
]

scene_choice_coord_system = [
    _("Transform vertices into world coordinate system"),
    _("Keep vertices into local coordinate system"),
]

scene_choice_asc_export = [
    _("Export mesh"),
    _("Export Animation"),
]

scene_choice_replace_merge = [
    _("Replace current model's skin with an imported skin"),
    _("Merge current model's skin with an imported skin")
]

scene_choice_anim_import = [
    _("Completely replace current model's animation"),
    _("Merge imported animation with current model's animation")
]

scene_choice = [
    _("Completely replace current scene"),
    _("Merge imported objects with current scene")
]

scene_choice_morphmesh = [
    scene_choice[0],
    scene_choice[1],
    _("Link root of imported object to the slot"),
    _("Link root of imported object to the bone"),
]

scene_choice_3ds_import = [
    scene_choice[0],
    scene_choice[1],
    _("Replace a slot with the imported object"),
    scene_choice_morphmesh[3], # Note: You can't just REPLACE a BONE object with a MESH object, that's impossible in blender!!!
]
limit_3ds_desc_text = _("Due to the inherent limitations of the 3DS format and this exporter's conversion process, it is not possible to export this mesh in its current state. To ensure successful exportation, please modify your mesh such that both the triangle count and vertex count do not exceed 65535.")
cant_divide_text = _("Can't divide.")
objects_to_export_text = _("Objects to export")
create_a_model_text = _("Create a new model")
merge_with_model_text = _("Merge with an existing model")
model_info_text = _("Model information")
anim_info_text = _("Animation information")
vertices_text = _("Vertices in scene")
trifaces_text = _("Tris")
verts_in_file_text = _("Vertices in file")
objects_text = _("Objects")
meshes_text = _("Meshes")
slots_text = _("Slots")
bones_text = _("Bones")
selected_text = _("Selected")
frame_rate_text = _("FPS")
bones_and_slots_text = _("Bones and slots")
for_import_text = _("For import")
for_export_text = _("For export")
information_text = _("Information")
empty_text = _("(Empty)")
sectored_materials_text = _("Remove sectored materials")
sectored_materials_desc = _("This option examines the BSP sectored interior materials imported from the world, starting with the prefix 'S:'. It removes these materials and reassigns them to the normal materials.")
bone_style_text = _("Bone style:")
bone_styles = ["Octahedral", "Stick", "BBone", "Envelope", "Wire"] # do not translate.
connect_bones_text = _("Try to connect bones")
allow_german_characters_text = _("Allow non-ascii Ö and Ü")
nonascii_characters_desc = _("There is a peculiar bug within the zSTRING class caused by its character table implementation. This class is unable to transform uppercase Ö and Ü characters into their lowercase counterparts ö and ü, which poses a problem in Gothic 1. Piranha Bytes, the developers of Gothic 1, did not adhere to a proper naming convention and used German characters in material and portal names. Therefore, please use this option exclusively with vanilla Gothic 1 meshes. Do NOT use it with Gothic 2.")
nonascii_characters_detected_text = _("Non-ASCII characters have been detected in the object's name or material slots. You need to remove or replace them.")
slot_transparency_text = _("Slot transparency: ")
use_sample_meshes_text = _("Use sample meshes from folder")
model_to_export_text = _("Model to export")
type_of_export_text = _("Type of export")
settings_changed_text = _("The settings have been changed. Please apply the changes and restart")
scale_and_time_transform_text = _("Space and Time Transformation")
auto_text = _("Auto")
model_prefix_text = _("Model prefix")
model_type_text = _("Model type")
parameters_text = _("Parameters")
rename_materials_text = _("Rename materials")
unknown_materials_checkbox_text = _("Assign names to unknown materials based on the file name of the diffuse texture map.")
unknown_materials_text = _("Unknown Materials")
known_materials_text = _("Known Materials")
load_material_filter_text = _("Load Materialfilter")
material_autorenaming_text = _("Material autorenaming")
materialfilter_text = _("Materialfilter")
invalid_matlib_path_text = _("Invalid matlib.ini file or path.")
material_text = _("Material")
texture_text = _("Texture")
path_to_matlib_text = _('Path to "matlib.ini"')
coordinate_system_text = _("Coordinate System")
diffuse_color_adjustment_text = _("Material diffuse color saturation: ")
model_type_choice = {
    ModelType.DYNAMIC: _("Dynamic model"),
    ModelType.STATIC: _("Static model"),
    ModelType.MORPH: _("MorphMesh model"),
}

scale_coefficient_desc = _("Scaling is calculated as a ratio between importing and exporting an object. This means that the scale value you have chosen during import will be reciprocated when exporting. For example, if you import with a scale of 0.01 meters, it will be exported with a scale of 100 meters.")

model_prefix_desc = _("A model prefix is a string which is inserted before any object's name. By default the model prefix is an empty string. The model prefix can be used for importing and editing more than one model in the same scene. Press the button to generate based on the objects of current scene or type one yourself.")
model_prefix_merging_desc = _("The following list contains prefixes of models which can be skinned with meshes imported from the file. These prefixes are listed for appropriate models only. An appropriate model is a model which has all the same bones' and slots' names as in the model imported from the file. Choose a model prefix.")

model_prefix_anim_intro = _("The following list contains prefixes of models which can be animated. These prefixes are listed for appropriate models only.")
model_prefix_dynamic_anim_desc = f"{model_prefix_anim_intro} {_('An appropriate model is a model which has the all the same bones and slots names as in the animation imported from the file. Choose a model prefix:')}"
model_morphmesh_desc = _(f"The following list contains all compatible morphmeshes with the animation provided above. An appropriate model is a model which has the same number of vertices as in the imported animation from file. Choose the morphmesh model:")

weld_threshold_text = _("Threshold (cm)")
destructive_action_tooltip = _("This action is destructive, it might corrupt your mesh/object.")
post_import_event_text = _("Post-Import Events")

choose_slot_text = _("Choose a slot")
choose_bone_text = _("Choose a bone")
# scale
scale_space_transformation_text = _("Space Transformation")
scale_button_label_noval_text = _("Scale")
scale_button_label_text = f"{scale_button_label_noval_text}: {default_scale_value_import}" # this doesn't need translation
scale_unit_text = _("Unit")
scale_coefficient_text = _("Scale coefficient")
# time
time_button_label_noval_text = _("Frames")
time_space_transformation_text = _("Time Transformation")
index_file_frame_text = _("Frame index in file")
index_scene_frame_text = _("Frame index in scene")
map_frames_text = _("Map file frames to scene frames")
first_processed_frame_text = _("First processed frame")
second_processed_frame_text = _("Second processed frame")
third_processed_frame_text = _("Third processed frame")
last_processed_frame_text = _("Last processed frame")
file_frames_text = _("File frames")
scene_frames_text = _("Scene frames")
first_frame_text = _("First frame")
second_frame_text = _("Second frame")
third_frame_text = _("Third frame")
last_frame_text = _("Last frame")
# Closing buttons
gothic_defaults_text = _("Gothic Defaults")
help_text = _("Help")
ok_text = _("OK")
cancel_text = _("Cancel")
import_text = _("Import")
export_text = _("Export")
options_text = _("Options")
select_all_text = _("Select all")
select_none_text = _("Deselect all")
# tooltips
tooltip_path_name = _("You can change the display of file path or name with the button")
tooltip_cannot_export_empty_list = _("Cannot export, list is empty.")
tooltip_cannot_import_prefix_exists = _("Prefix already exists in scene!")
tooltip_cannot_import_multiscene_prefix_exists = _("Prefix already exists in different scenes!")


asc_export_help_static_model = _("Static models can be used for modelling beds, anvils, ladders, idols, chairs, and other objects, which can move only entirely (i.e. any part of an object is immovable relative to any other part of the same object). A static model is represented by one ASC file. A compiled static model is represented by one MDL file.")
asc_export_help_dynamic_model = _("Dynamic models can be used for modelling humans, orcs, monsters, their overlays, doors, chests and other objects which have skeletons. A dynamic model is represented by one MDS file and two or more ASC files. A compiled dynamic model is represented by one MDH file, one or more MDM files, and one or more MAN files. Export of any dynamic model contains two steps:\n1) export a model in initial pose\n2) export model's animations")

asc_export_help_dynamic_model_mode = [
    _("\n\nCurrent mode: Export mesh.\n\nYou should write the ASC file name to the \"meshAndTree\" or \"RegisterMesh\" section of your MDS file.\nThere are two choices:\n1) Press the \"All\" button to export the dynamic model entirely\n2) Press \"None\" button to export bones and slots only (meshes aren't exported in this case).\nThe last choice can be useful if you want to save an overlay's skeleton."),
    _("\n\nCurrent mode: Export animation.\n\nYou should write the ASC file name to the \"ani\" section of your MDS file. There are two choices:\n1) Press the \"All\" button to export animation of all the bones and slots\n2) Select bones and slots to export animation of some part of bones and slots\n\nYou should use the last choice if you want to save animation for the second layer. Here is a short explanation. Animations of different layers can be overlayed. For example, animation of sword\'s drawing is overlayed on animation of running man and the result is sword\'s drawing while running. Therefore the animation of running man is at the first layer (export animation of all bones and slots) and the sword\'s drawing is at the second later (export animation of subtree starting with the \"Bip01 Spine1\" object)."),
]

asc_export_help_morphmeshes = _("MorphMeshes can be used for modelling heads, bows, crossbows, flags, and so on. MorphMesh is represented by one MMS file and two or more ASC files. A compiled MorphMesh is represented by one MMB file. Export of any MorphMesh contains two steps:\n1) export an initial mesh\n2) export meshes animations")
asc_export_help_morphmesh_mode = [
    _("\n\nCurrent mode: Export mesh.\n\nYou should write the ASC file name to the \"morphRef\" section of your MMS file."),
    _("\n\nCurrent mode: Export animation.\n\nYou should write the ASC file name to the \"morphAni\" section of your MMS file."),
]

# Error handling + generic error messages

error_text = "ERROR!"
error_messages = [
    f"{_('Seek professional help at')} {WEBSITE}-/issues",
    _("Error while parsing json data from plugin!"),
    _("No valid dynamic models were found for this animation to import."),
    _("No valid MorphMesh models were found for this animation to import."),
    _("No valid models to export."),
    _("Can't run tool, illegal object name found. Faulty name:"),
    _("Invalid format. Not a Gothic ZEN, ASC, MRM, 3DS or MSH file.")
]