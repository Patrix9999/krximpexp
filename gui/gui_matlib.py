import operator
from pathlib import Path

import dearpygui.dearpygui as dpg
import gui_values as val
from gui_generics import file_picker_callback
from gui_system import config
from gui_window_handling import LayoutHelper, define_size, generic_modal_cfg, render_window_center

class MatLibParserUI:
    __slots__ = ("__tmaterials")

    def __init__(self, filepath):
        self.__tmaterials = list()
        self.load_material_filter(filepath)

    @property
    def materials(self) -> list:
        if self.__tmaterials:
            if len(self.__tmaterials) > 0:
                return self.__tmaterials
        return None

    def get_material(self, idx) -> dict:
        if self.__tmaterials:
            if len(self.__tmaterials) > 0:
                return self.__tmaterials[idx]
        return None

    def set_material(self, idx, value):
        if self.__tmaterials:
            if len(self.__tmaterials) > 0:
                self.__tmaterials[idx] = value

    # Only for frontend, faster, but gathers only 2 values
    def __pml_parse(self, pml_file_path):
        try:
            with open(pml_file_path, "rt", encoding="Windows-1250") as file_handle:
                name = ""
                texture = ""
                for line in file_handle:
                    line = line.rstrip("\r\n")
                    if "[% zCMaterial" in line:
                        name = ""
                        texture = ""
                    elif "name=string:" in line:
                        name = line[line.find(":") + 1 :]
                    elif "texture=string:" in line:
                        texture = line[line.find(":") + 1 :]
                    elif line.find("[]") != -1:
                        if name or texture:
                            tmaterial = {"name": name, "texture": texture}
                            self.__tmaterials.append(tmaterial)
        except (EnvironmentError, AttributeError) as e:  # parent of IOError, OSError *and* WindowsError where available
            self.__tmaterials = list()

    def load_material_filter(self, mat_lib_ini_path: str):
        if mat_lib_ini_path:
            if len(self.__tmaterials) > 0:
                self.__tmaterials = list()
            try:
                with open(mat_lib_ini_path, "rt", encoding="Windows-1250") as file_handle:
                    self.__tmaterials = []
                    for line in file_handle:
                        line = line.rstrip("\r\n")
                        pos = line.find("=")
                        if pos != -1:
                            pml_file_path = Path(mat_lib_ini_path).parent / f"{line[:pos]}.pml"
                            self.__pml_parse(pml_file_path)

                    self.__tmaterials.sort(key=operator.itemgetter("name"))
            # parent of IOError, OSError *and* WindowsError where available
            except (EnvironmentError, AttributeError) as e:
                self.__tmaterials = list()


def load_material_filter_callback(sender, app_data, user_data):
    matlib = MatLibParserUI(dpg.get_value("matlibini_path_input"))
    dpg.delete_item("invalid_matlib_message")
    dpg.delete_item("matlib_table")

    if matlib.materials:
        dpg.add_table(parent="matlib_window_helper", tag="matlib_table", header_row=True, clipper=True)
        dpg.add_table_column(parent="matlib_table", label="#", width_fixed=True, width=define_size(10))
        dpg.add_table_column(parent="matlib_table", label=val.material_text)
        dpg.add_table_column(parent="matlib_table", label=val.texture_text)
        for i, material in enumerate(matlib.materials):
            with dpg.table_row(parent="matlib_table"):
                dpg.add_text(str(i + 1))
                dpg.add_input_text(default_value=str(material["name"]), width=-1, enabled=False)  # mat_name
                dpg.add_input_text(default_value=str(material["texture"]), width=-1, enabled=False)  # mat_texture
    else:
        dpg.add_text(val.invalid_matlib_path_text, tag="invalid_matlib_message", parent="matlib_window_helper")


def matlib_cancel_callback():
    dpg.configure_item("matlib_popup", show=False)
    dpg.set_value("unknown_materials_checkbox", False)
    dpg.set_value("matlibini_path_input", "")
    dpg.delete_item("invalid_matlib_message")
    dpg.delete_item("matlib_table")


def matlib_ok_callback():
    dpg.configure_item("matlib_popup", show=False)
    config.handle["MATLIB"]["matlibini_path"] = dpg.get_value("matlibini_path_input")
    config.handle["MATLIB"]["autorenaming"] = dpg.get_value("rename_materials_checkbox")


def matlib_parameters_enabled_callback(sender, app_data, user_data):
    dpg.configure_item("rename_materials_parameters_button", enabled=app_data)


def init_matlib_popup(matlibini_path_from_user=None):
    matlib_modal_cfg = generic_modal_cfg
    matlib_modal_cfg["no_title_bar"] = True
    matlib_modal_cfg["height"] = define_size(575)
    matlib_modal_cfg["width"] = define_size(620)
    matlibini_path_settings = config.handle["MATLIB"]["matlibini_path"]
    with dpg.window(tag="matlib_popup", **matlib_modal_cfg):
        dpg.add_text(val.materialfilter_text)
        line = LayoutHelper()
        line.add_widget(dpg.add_button(label=val.path_to_matlib_text), 30)
        line.add_widget(dpg.add_input_text(tag="matlibini_path_input"), 60)
        if matlibini_path_settings:
            dpg.set_value("matlibini_path_input", matlibini_path_settings)
        if matlibini_path_from_user:
            dpg.set_value("matlibini_path_input", matlibini_path_from_user)
        line.add_widget(dpg.add_button(label="...", callback=file_picker_callback), 10)
        line.submit()
        dpg.add_button(
            label=val.load_material_filter_text,
            tag="load_materialfilter_button",
            callback=load_material_filter_callback,
            width=-1,
        )
        dpg.add_separator()
        dpg.add_text(val.known_materials_text)
        dpg.add_child_window(tag="matlib_window_helper", height=define_size(300))
        dpg.add_text(val.unknown_materials_text)
        dpg.add_checkbox(label=val.unknown_materials_checkbox_text, tag="unknown_materials_checkbox")

        line = LayoutHelper()
        line.add_widget(dpg.add_button(label=val.ok_text, callback=matlib_ok_callback), 50)
        line.add_widget(dpg.add_button(label=val.cancel_text, callback=matlib_cancel_callback), 50)
        line.submit()


def init_matlib_buttons(matlibini_path_from_user):
    def callback():
        with dpg.mutex():
            dpg.configure_item("matlib_popup", show=True)
            render_window_center("matlib_popup")

    autorenaming_setting = config.handle["MATLIB"]["autorenaming"]
    init_matlib_popup(matlibini_path_from_user)
    dpg.add_text(val.material_autorenaming_text)
    with dpg.group(horizontal=True):
        dpg.add_checkbox(
            label=val.rename_materials_text,
            tag="rename_materials_checkbox",
            callback=matlib_parameters_enabled_callback,
        )
        dpg.add_button(
            label=val.parameters_text,
            tag="rename_materials_parameters_button",
            callback=callback,
            width=-1,
            enabled=False,
        )
        if autorenaming_setting:
            dpg.set_value("rename_materials_checkbox", True)
            dpg.configure_item("rename_materials_parameters_button", enabled=True)
