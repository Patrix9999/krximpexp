import gui_values as val
from gui_system import JsonHandler

try:
    import dearpygui.dearpygui as dpg

    # import dearpygui.demo as demo
except (ModuleNotFoundError, ImportError) as e:  # TODO: Selfhealing
    raise Exception(e)

from gui_generics import init_confirmation_buttons, init_file_properties, init_time_and_scale_transform
from gui_window_handling import LayoutHelper, WindowProperties, define_size, initialize_context, window_handling
from message_box import gui_messagebox

from helpers import ExportStatus, ModelType, ObjectType, find_children_by_parent_name, sort_relations


def krx_export(title=val.window_title_export_asc):
    def export_callback(sender, app_data, user_data):
        unique_id, model_hierarchies, model_prefixes = user_data
        status = ExportStatus()
        status.color_adjustment = dpg.get_value("color_adjustment_slider")

        objects_to_export = list()
        model_prefix = dpg.get_value("model_prefixes_combo_button")
        export_type = dpg.get_value("scene_radio_button")
        hierarchy = model_hierarchies[model_prefixes.index(model_prefix)]

        for obiekt in hierarchy["objects"]:
            current_tag = f"{model_prefix}{obiekt}{unique_id}"
            current_checkbox = f"{current_tag}_checkbox"
            if dpg.does_item_exist(current_checkbox) and dpg.get_value(current_checkbox):
                objects_to_export.append(obiekt)

        status.selected_objects = objects_to_export
        status.export_animation = bool(export_type == val.scene_choice_asc_export[1])
        status.scale = float(dpg.get_value("scale_input"))
        if status.export_animation:
            status.start_frame_in_scene = int(dpg.get_value("first_scene_frame_input"))
            status.end_frame_in_scene = int(dpg.get_value("last_scene_frame_input"))
            status.start_frame_in_file = int(dpg.get_value("first_file_frame_input"))
            status.end_frame_in_file = int(dpg.get_value("last_file_frame_input"))
        if model_prefix == val.empty_text:
            model_prefix = ""
        status.selected_prefix = model_prefix
        status.send()

    def assemble_treectrl(window_handle="hierarchy_window", parent_window="layout_right", data=None):
        if data:
            (
                unique_id,
                hierarchy,
                model_prefix,
                sorted_relations,
                morph_mesh,
                morph_anim,
                static_mesh,
                dynamic_mesh,
                dynamic_anim,
            ) = data

            if dpg.does_item_exist(window_handle):
                dpg.delete_item(window_handle)
            dpg.add_child_window(parent=parent_window, tag=window_handle, width=-1, height=-1)

            for j, obiekt in enumerate(hierarchy["objects"]):
                object_type = hierarchy["object_types"][j]
                if not (
                    dynamic_anim and object_type == ObjectType.MESH
                ):  # Do not create mesh entries in ctrltree when in animation mode
                    parent = hierarchy["object_parents"][j]
                    indent = 0

                    parent_tag = f"{model_prefix}{parent}{unique_id}"
                    parent_group = f"{parent_tag}_group"
                    parent_button = f"{parent_tag}_button"
                    parent_checkbox = f"{parent_tag}_checkbox"

                    current_tag = f"{model_prefix}{obiekt}{unique_id}"
                    current_group = f"{current_tag}_group"
                    current_button = f"{current_tag}_button"
                    current_checkbox = f"{current_tag}_checkbox"
                    if dpg.does_item_exist(parent_group):
                        parent_indent = dpg.get_item_indent(parent_group)
                        indent = parent_indent + 30  # this cannot be scaled by define_size!, will bug out...
                    with dpg.group(horizontal=True, parent=window_handle, tag=current_group, indent=indent):
                        children_list = find_children_by_parent_name(model_prefix, obiekt, sorted_relations)
                        if children_list:
                            dpg.add_button(
                                label="-",
                                width=define_size(20),
                                tag=current_button,
                                callback=expand_collapse_button_callback,
                                user_data=(unique_id, hierarchy, obiekt, False),
                            )
                        else:
                            dpg.add_spacer(width=define_size(20))

                        dpg.add_checkbox(
                            label=obiekt,
                            tag=current_checkbox,
                            user_data=(unique_id, hierarchy),
                            callback=selected_objects_checkbox_counter_callback,
                        )
                        if morph_mesh or morph_anim or static_mesh:
                            dpg.configure_item(current_checkbox, default_value=True, enabled=False)
                        elif dynamic_mesh:
                            val = not (object_type == ObjectType.BONE or object_type == ObjectType.SLOT)
                            dpg.configure_item(current_checkbox, default_value=True, enabled=val)
                        elif dynamic_anim:
                            val = object_type == ObjectType.BONE or object_type == ObjectType.SLOT
                            dpg.configure_item(current_checkbox, default_value=True, enabled=val)

    def type_of_export_changed_callback(sender, app_data, user_data):
        unique_id, model_hierarchies, model_prefixes = user_data
        model_prefix = dpg.get_value("model_prefixes_combo_button")
        export_type = dpg.get_value("scene_radio_button")

        hierarchy = model_hierarchies[model_prefixes.index(model_prefix)]
        model_type = hierarchy["model_type"]
        time_transform_toggle = export_type == val.scene_choice_asc_export[1]

        morph_mesh = not time_transform_toggle and model_type == ModelType.MORPH
        morph_anim = time_transform_toggle and model_type == ModelType.MORPH
        static_mesh = model_type == ModelType.STATIC
        dynamic_mesh = not time_transform_toggle and model_type == ModelType.DYNAMIC
        dynamic_anim = time_transform_toggle and model_type == ModelType.DYNAMIC

        if morph_mesh or morph_anim:
            help_description = val.asc_export_help_morphmeshes
            if morph_mesh:
                help_description += val.asc_export_help_morphmesh_mode[0]
            else:
                help_description += val.asc_export_help_morphmesh_mode[1]
        elif static_mesh:
            help_description = val.asc_export_help_static_model
        elif dynamic_mesh or dynamic_anim:
            help_description = val.asc_export_help_dynamic_model
            if dynamic_mesh:
                help_description += val.asc_export_help_dynamic_model_mode[0]
            else:
                help_description += val.asc_export_help_dynamic_model_mode[1]

        dpg.set_value("help_description", help_description)
        dpg.set_value("model_type_input", val.model_type_choice[model_type])
        dpg.configure_item("time_transform_button", enabled=time_transform_toggle, show=time_transform_toggle)

        assemble_treectrl(
            data=(
                unique_id,
                hierarchy,
                model_prefix,
                sorted_relations,
                morph_mesh,
                morph_anim,
                static_mesh,
                dynamic_mesh,
                dynamic_anim,
            )
        )
        selection_callback(sender, app_data, (unique_id, model_hierarchies, model_prefixes, True))

    def model_prefix_changed_callback(sender, app_data, user_data):
        type_of_export_changed_callback(sender, app_data, user_data)

    # TODO: figure out a way to store children branch stuff, and hide it after a branch above was expanded
    def expand_collapse_button_callback(sender, app_data, user_data):
        unique_id, hierarchy, obiekt, toggle = user_data
        model_prefix = hierarchy["model_prefix"]
        if sender == f"{model_prefix}{obiekt}{unique_id}_button":
            children_list = find_children_by_parent_name(model_prefix, obiekt, sorted_relations)
            if children_list:
                for obj in children_list:
                    group_tag = f"{model_prefix}{obj}{unique_id}_group"
                    if dpg.does_item_exist(group_tag):
                        dpg.configure_item(group_tag, show=toggle)
                toggle = not toggle
                dpg.set_item_user_data(sender, (unique_id, hierarchy, obiekt, toggle))
                dpg.configure_item(sender, label="+" if toggle else "-")

    def selected_objects_checkbox_counter_callback(sender, app_data, user_data):
        unique_id, hierarchy = user_data
        model_prefix = dpg.get_value("model_prefixes_combo_button")
        if isinstance(sender, str) and sender.find(unique_id) != -1:
            selected_object = sender.removeprefix(model_prefix).removesuffix(f"{unique_id}_checkbox")
            if app_data:
                if selected_object not in selected_objects:
                    selected_objects.add(selected_object)
            else:
                if selected_object in selected_objects:
                    selected_objects.remove(selected_object)

        count = len(selected_objects)

        if count <= 0:
            dpg.configure_item("confirm_button", enabled=False)
            dpg.configure_item(
                "selected_objects_count_button", label=f"{val.selected_text}: 0. {val.tooltip_cannot_export_empty_list}"
            )
        else:
            dpg.configure_item("confirm_button", enabled=True)
            dpg.configure_item("selected_objects_count_button", label=f"{val.selected_text}: {count}")

    def selection_callback(sender, app_data, user_data):
        unique_id, model_hierarchies, model_prefixes, switch = user_data
        model_prefix = dpg.get_value("model_prefixes_combo_button")
        hierarchy = model_hierarchies[model_prefixes.index(model_prefix)]
        selected_objects.clear()
        for obiekt in hierarchy["objects"]:
            current_tag = f"{model_prefix}{obiekt}{unique_id}"
            current_checkbox = f"{current_tag}_checkbox"
            if dpg.does_item_exist(current_checkbox):
                if dpg.get_item_configuration(current_checkbox)["enabled"]:
                    dpg.set_value(current_checkbox, switch)
                if dpg.get_value(current_checkbox):
                    selected_objects.add(obiekt)
        selected_objects_checkbox_counter_callback(sender, app_data, (unique_id, hierarchy))

    tool_mode = "EXPORT"
    plugin_data = JsonHandler.load(tool=f"INPUT_{tool_mode}")
    file_name = plugin_data["file_path"]
    scene_mode = f"{tool_mode}_ASC"
    win_width = 1000
    win_height = 730

    model_hierarchies = plugin_data["model_hierarchies"]
    if len(model_hierarchies) == 0:
        gui_messagebox(file_name=file_name, message=val.error_messages[4], title=val.information_text) # No valid export
        exit()

    model_prefixes = list()
    selected_objects = set(plugin_data["selected_objects"])

    # housekeeping.
    for hierarchy in model_hierarchies:
        if hierarchy["model_prefix"] == val.empty_text:
            gui_messagebox(file_name=file_name, error_message=f"{val.error_messages[5]} {hierarchy['model_prefix']}")
            exit()
        elif hierarchy["model_prefix"] == "":
            hierarchy["model_prefix"] = val.empty_text
        for i, parent in enumerate(hierarchy["object_parents"]):
            if parent == "":
                hierarchy["object_parents"][i] = "root"
        model_prefixes.append(hierarchy["model_prefix"])

    sorted_relations = sort_relations(model_hierarchies)

    if plugin_data["selected_prefix"] is not None:
        if plugin_data["selected_prefix"] == "":
            plugin_data["selected_prefix"] = val.empty_text

    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(win_width), "h": define_size(win_height)}

    initialize_context()
    unique_id = f"_@@_{dpg.generate_uuid()}"

    # main window
    with dpg.window(tag="WINDOW_PANEL"):
        child_window_height = 600

        with dpg.group(tag="window_layout", horizontal=True):
            with dpg.child_window(
                tag="layout_left",
                width=define_size(400),
                height=define_size(child_window_height),
                no_scrollbar=True,
                horizontal_scrollbar=False,
            ):
                dpg.add_text(val.model_to_export_text)
                line = LayoutHelper()
                line.add_widget(dpg.add_button(label=f"{val.model_prefix_text}:"), 25)
                line.add_widget(
                    dpg.add_combo(
                        items=model_prefixes,
                        callback=model_prefix_changed_callback,
                        tag="model_prefixes_combo_button",
                        user_data=(unique_id, model_hierarchies, model_prefixes),
                    ),
                    75,
                )
                line.submit()

                line = LayoutHelper()
                line.add_widget(dpg.add_button(label=f"{val.model_type_text}:"), 25)
                line.add_widget(dpg.add_input_text(default_value="", tag="model_type_input", enabled=False), 75)
                line.submit()

                dpg.add_separator()
                dpg.add_text(val.type_of_export_text)
                dpg.add_radio_button(
                    tag="scene_radio_button",
                    items=val.scene_choice_asc_export,
                    callback=type_of_export_changed_callback,
                    user_data=(unique_id, model_hierarchies, model_prefixes),
                    horizontal=True,
                )
                dpg.add_separator()
                dpg.add_text(val.scale_and_time_transform_text)
                init_time_and_scale_transform(plugin_data, mode=tool_mode, parent="layout_left")
                dpg.add_separator()
                dpg.add_text(val.information_text)
                with dpg.child_window(border=False):
                    dpg.add_text(wrap=define_size(400 - 50), tag="help_description")

            with dpg.child_window(
                tag="layout_right",
                width=-1,
                height=define_size(child_window_height) + 1,
                border=False,
                no_scrollbar=True,
                horizontal_scrollbar=False,
            ):
                line = LayoutHelper()
                line.add_widget(
                    dpg.add_button(
                        label=val.select_all_text,
                        callback=selection_callback,
                        user_data=(unique_id, model_hierarchies, model_prefixes, True),
                    ),
                    25,
                )
                line.add_widget(
                    dpg.add_button(
                        label=val.select_none_text,
                        callback=selection_callback,
                        user_data=(unique_id, model_hierarchies, model_prefixes, False),
                    ),
                    25,
                )
                line.submit()
                dpg.add_button(
                    label=f"{val.selected_text}: {len(selected_objects)}", width=-1, tag="selected_objects_count_button"
                )

        init_file_properties(file_name, mode=tool_mode)
        init_confirmation_buttons(export_callback, mode=tool_mode)
        dpg.set_item_user_data("confirm_button", (unique_id, model_hierarchies, model_prefixes))

    def setup_callback():
        if plugin_data["selected_prefix"]:
            dpg.set_value("model_prefixes_combo_button", plugin_data["selected_prefix"])
            model_prefix_changed_callback(
                "setup", plugin_data["selected_prefix"], (unique_id, model_hierarchies, model_prefixes)
            )

    window_handling(window_handler, title, custom_callback=(setup_callback,))


if __name__ == "__main__":
    krx_export()
