import gui_values as val
from gui_system import JsonHandler

try:
    import dearpygui.dearpygui as dpg
except ImportError as e:  # TODO: Selfhealing
    raise Exception(e)

import textwrap

from gui_generics import init_file_properties, options_button
from gui_window_handling import LayoutHelper, WindowProperties, define_size, initialize_context, window_handling


def gui_messagebox(
    file_name=None, options=False, title=val.error_text, message=None, mode="MESSAGE_BOX"
):

    if not message:
        plugin_data = JsonHandler.load(tool=mode)
        message = plugin_data["error_message"]

    if title == val.error_text:
        message = f"{message}\n\n\n{val.error_messages[0]}"

    wrapper = textwrap.TextWrapper(width=define_size(100), replace_whitespace=False, drop_whitespace=False)
    message = wrapper.fill(text=message)

    initialize_context()

    with dpg.window(tag="WINDOW_PANEL"):
        if file_name:
            init_file_properties(file_name)
        dpg.add_input_text(default_value=message, width=-1, enabled=False, multiline=True, tag="message_text")

        line = LayoutHelper()
        line.add_widget(dpg.add_spacer(), 20)
        line.add_widget(dpg.add_button(label=val.cancel_text, callback=lambda: dpg.stop_dearpygui()), 30)
        if options:
            line.add_widget(options_button(), 30)
        line.add_widget(dpg.add_spacer(), 20)
        line.submit()

    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(600), "h": define_size(355 if file_name else 255)}
    window_handling(window_handler, title)


if __name__ == "__main__":
    gui_messagebox()
