import webbrowser
import dearpygui.dearpygui as dpg
import gui_values as val
from gui_system import config, SYSTEM
from gui_window_handling import LayoutHelper, define_size, generic_modal_cfg


def settings_changed_callback():
    dpg.set_value("settings_changed", val.settings_changed_text)
    dpg.configure_item("options_apply_button", label=val.ok_text)
    dpg.set_item_user_data("options_apply_button", True)


def options_apply_callback(sender, app_data, user_data):
    if user_data:
        with dpg.mutex():
            config.handle["OPTIONS"]["language"] = val.language_handler.language_texts_to_codes[
                dpg.get_value("languages_list_combobutton")
            ]
            config.handle["OPTIONS"]["dpi_multiplier"] = dpg.get_value("dpi_multiplier_input")
            config.handle["SCENE"]["color_adjustment"] = dpg.get_value("color_adjustment_slider")
            print("RESTART_GUI")
            dpg.stop_dearpygui()
    else:
        dpg.configure_item("options_popup", show=False)


def options_help_callback():
    if SYSTEM == "WINDOWS":
        webbrowser.get("windows-default").open(val.WEBSITE)
    elif SYSTEM == "DARWIN":
        webbrowser.get("macos").open(val.WEBSITE)
    else:  # Let's just pray for the best as there's no sane default for freetards...
        webbrowser.open(val.WEBSITE)


def init_options_popup():
    options_modal_cfg = generic_modal_cfg
    options_modal_cfg["height"] = define_size(175)
    options_modal_cfg["no_title_bar"] = True
    with dpg.window(tag="options_popup", **options_modal_cfg):
        with dpg.group(horizontal=True):
            lang = val.language_handler.language_codes_to_texts[config.handle["OPTIONS"]["language"]]

            line = LayoutHelper()
            line.add_widget(dpg.add_button(label=f"{val.language_text}:"), 25)
            line.add_widget(
                dpg.add_combo(
                    items=val.language_handler.language_texts,
                    default_value=lang,
                    width=-1,
                    callback=settings_changed_callback,
                    tag="languages_list_combobutton",
                ),
                75,
            )
            line.submit()

        with dpg.group(horizontal=True):
            dpi_multiplier = float(config.handle["OPTIONS"]["dpi_multiplier"])

            line = LayoutHelper()
            line.add_widget(dpg.add_button(label=f"{val.interface_size_text}:"), 25)
            line.add_widget(
                dpg.add_input_double(
                    default_value=dpi_multiplier,
                    width=-1,
                    callback=settings_changed_callback,
                    tag="dpi_multiplier_input",
                ),
                75,
            )
            line.submit()

        with dpg.group(horizontal=True):
            dpg.add_text(val.diffuse_color_adjustment_text)
            dpg.add_slider_int(
                tag="color_adjustment_slider",
                default_value=config.handle["SCENE"]["color_adjustment"],
                max_value=100,
                min_value=0,
                format="%i%%",
                width=-1,
                callback=settings_changed_callback,
            )

        line = LayoutHelper()
        line.add_widget(dpg.add_spacer(), 25)
        line.add_widget(
            dpg.add_button(
                tag="options_apply_button", label=val.cancel_text, callback=options_apply_callback, user_data=False
            ),
            33,
        )
        line.add_widget(dpg.add_button(label=val.help_text, callback=options_help_callback), 33)
        line.add_widget(dpg.add_spacer(), 25)
        line.submit()

        dpg.add_text(tag="settings_changed")
