import gui_values as val
from gui_system import JsonHandler, config

try:
    import dearpygui.dearpygui as dpg
except (ModuleNotFoundError, ImportError) as e:  # TODO: Selfhealing
    raise Exception(e)

from gui_generics import asc_animation_stats, init_confirmation_buttons, init_file_properties
from gui_scale import init_scale_button
from gui_time_transform import init_time_transform
from gui_window_handling import WindowProperties, define_size, initialize_context, window_handling
from message_box import gui_messagebox

from helpers import ImportStatus, SceneMode


def krx_import(title=val.window_title_import_asc_dynamic_ani):
    def import_callback():
        status = ImportStatus()
        scene_mode = dpg.get_value("scene_radio_button")
        model_prefix = dpg.get_value("scene_prefix_anim_button")

        if model_prefix == val.empty_text:
            model_prefix = ""

        if scene_mode == val.scene_choice_anim_import[0]:
            status.scene_mode = SceneMode.REPLACE_ANIM
        elif scene_mode == val.scene_choice_anim_import[1]:
            status.scene_mode = SceneMode.MERGE_ANIM

        status.selected_prefix = model_prefix

        status.start_frame_in_file = int(dpg.get_value("first_file_frame_input"))
        status.end_frame_in_file = int(dpg.get_value("last_file_frame_input"))
        status.start_frame_in_scene = int(dpg.get_value("first_scene_frame_input"))
        status.end_frame_in_scene = int(dpg.get_value("last_scene_frame_input"))
        status.scale = float(dpg.get_value("scale_input"))

        config.handle["SCENE"]["import_mode"] = int(status.scene_mode)
        status.send()

    tool_mode = "IMPORT"
    plugin_data = JsonHandler.load(tool=f"INPUT_{tool_mode}")
    file_name = plugin_data["file_path"]
    win_width = 600
    win_height = 600

    if plugin_data["appropriate_prefixes"]:
        for i, prefix in enumerate(plugin_data["appropriate_prefixes"]):
            if prefix == val.empty_text:
                gui_messagebox(file_name=file_name, error_message=f"{val.error_messages[5]} {prefix}")
                exit()
            if prefix == "":
                if plugin_data["selected_prefix"] == prefix:
                    plugin_data["selected_prefix"] = val.empty_text
                plugin_data["appropriate_prefixes"][i] = val.empty_text
    else:
        gui_messagebox(file_name=file_name, message=val.error_messages[2], title=val.information_text)
        exit()

    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(win_width), "h": define_size(win_height)}

    initialize_context()

    # main window
    with dpg.window(tag="WINDOW_PANEL"):
        init_file_properties(file_name)

        dpg.add_separator()
        asc_animation_stats(plugin_data)

        dpg.add_separator()
        with dpg.group():
            dpg.add_text(val.anim_todo_text)
            dpg.add_radio_button(
                items=val.scene_choice_anim_import, default_value=val.scene_choice_anim_import[0], tag="scene_radio_button"
            )

        dpg.add_separator()
        dpg.add_text(val.model_prefix_text)
        dpg.add_text(val.model_prefix_dynamic_anim_desc, wrap=window_handler.dimension["w"] - define_size(50))
        dpg.add_combo(
            items=plugin_data["appropriate_prefixes"],
            default_value=plugin_data["selected_prefix"],
            width=-1,
            tag="scene_prefix_anim_button",
        )

        dpg.add_separator()
        init_scale_button(mode=tool_mode)
        init_time_transform(plugin_data, mode=tool_mode)  # button

        dpg.add_separator()
        init_confirmation_buttons(import_callback, mode=tool_mode)

    window_handling(window_handler, title)


if __name__ == "__main__":
    krx_import()
