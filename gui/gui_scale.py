import dearpygui.dearpygui as dpg
import gui_values as val
from gui_system import config
from gui_window_handling import LayoutHelper, define_size, generic_modal_cfg, render_window_center


def get_scale_from_config(mode=None):
    if mode == "IMPORT":
        return float(config.handle["SCALE"]["system_units_per_file_unit"])
    elif mode == "EXPORT":
        return float(config.handle["SCALE"]["file_units_per_system_unit"])


def scale_input_callback(sender, app_data, user_data):
    try:
        dpg.set_value("scale_input_helper", 1 / float(app_data))
    except (ValueError, ZeroDivisionError) as e:
        dpg.set_value("scale_input_helper", f"{val.cant_divide_text} :(")


def scale_combo_callback():
    try:
        scale_option = val.scale_units.index(dpg.get_value("scale_combo"))
        dpg.set_value("scale_input", val.scale_units_value[scale_option])
        dpg.set_value("scale_input_helper", 1 / val.scale_units_value[scale_option])
    except ValueError:
        scale_reset_button_callback("scale_combo_callback", None, "IMPORT")



def scale_apply_button_callback(sender, app_data, user_data):
    mode = user_data
    try:
        scale = float(dpg.get_value("scale_input"))
    except ValueError:
        if mode == "IMPORT":
            scale = val.default_scale_value_import
        elif mode == "EXPORT":
            scale = val.default_scale_value_export

        scale_reset_button_callback(sender, app_data, user_data)

    if scale > 0:
        if mode == "IMPORT":
            try:
                scale_unit = val.scale_units.index(dpg.get_value("scale_combo"))
            except ValueError:
                scale_unit = val.default_scale

            config.handle["SCALE"]["setup_unit"] = scale_unit
            config.handle["SCALE"]["system_units_per_file_unit"] = scale
            config.handle["SCALE"]["file_units_per_system_unit"] = 1 / scale
        elif mode == "EXPORT":
            config.handle["SCALE"]["system_units_per_file_unit"] = 1 / scale
            config.handle["SCALE"]["file_units_per_system_unit"] = scale
        dpg.configure_item("scale_button", label=f"{val.scale_button_label_noval_text}: {scale}")
    else:
        scale_reset_button_callback(sender, app_data, user_data)

    dpg.configure_item("scale_popup", show=False)


def scale_reset_button_callback(sender, app_data, user_data):
    mode = user_data
    if mode == "IMPORT":
        dpg.set_value("scale_combo", val.scale_units[val.default_scale])
        dpg.set_value("scale_input", val.default_scale_value_import)
        dpg.set_value("scale_input_helper", 1 / val.default_scale_value_import)
    elif mode == "EXPORT":
        dpg.set_value("scale_input", val.default_scale_value_export)
        dpg.set_value("scale_input_helper", 1 / val.default_scale_value_export)


def init_scale_popup(mode=None):

    scale_modal_cfg = generic_modal_cfg
    if mode == "IMPORT":
        scale_modal_cfg["height"] = define_size(160)
    if mode == "EXPORT":
        scale_modal_cfg["height"] = define_size(110)

    scale_modal_cfg["no_title_bar"] = True

    with dpg.window(tag="scale_popup", **scale_modal_cfg):
        scale = get_scale_from_config(mode)
        scale_unit = config.handle["SCALE"]["setup_unit"]

        if mode == "IMPORT":
            with dpg.group(horizontal=True):
                dpg.add_button(label=f"{val.scale_unit_text}:")
                dpg.add_combo(
                    val.scale_units,
                    width=-1,
                    default_value=val.scale_units[scale_unit],
                    callback=scale_combo_callback,
                    tag="scale_combo",
                )

        dpg.add_button(label=f"{val.scale_coefficient_text}:", tag="scale_coefficient_text", width=-1)
        with dpg.tooltip("scale_coefficient_text"):
            dpg.add_text(val.scale_coefficient_desc, wrap=define_size(250))
        line = LayoutHelper()
        line.add_widget(
            dpg.add_button(label=f"{val.for_import_text}:" if mode == "IMPORT" else f"{val.for_export_text}:"), 10
        )
        # text input here because float input tends to overflow with imprecisions...
        line.add_widget(
            dpg.add_input_text(
                default_value=scale,
                width=define_size(20),
                decimal=True,
                no_spaces=True,
                tag="scale_input",
                callback=scale_input_callback,
            ),
            10,
        )
        line.add_widget(
            dpg.add_button(label=f"{val.for_export_text}:" if mode == "IMPORT" else f"{val.for_import_text}:"), 10
        )
        line.add_widget(
            dpg.add_input_text(
                default_value=1 / scale,
                width=define_size(20),
                decimal=True,
                enabled=False,
                no_spaces=True,
                tag="scale_input_helper",
            ),
            15,
        )
        line.submit()

        line = LayoutHelper()
        line.add_widget(dpg.add_spacer(), 10)
        line.add_widget(dpg.add_button(label=val.ok_text, callback=scale_apply_button_callback, user_data=mode), 10)
        # giving enough space for different translations
        line.add_widget(
            dpg.add_button(label=val.gothic_defaults_text, callback=scale_reset_button_callback, user_data=mode), 30
        )
        line.add_widget(dpg.add_spacer(), 10)
        line.submit()


def init_scale_button(mode=None):
    init_scale_popup(mode)

    def callback():
        with dpg.mutex():
            dpg.configure_item("scale_popup", show=True)
            render_window_center("scale_popup")

    with dpg.group(horizontal=True):
        line = LayoutHelper()
        line.add_widget(dpg.add_button(label=f"{val.scale_space_transformation_text}:"), 60)
        line.add_widget(
            dpg.add_button(
                label=f"{val.scale_button_label_noval_text}: {get_scale_from_config(mode)}",
                width=-1,
                callback=callback,
                tag="scale_button",
            ),
            40,
        )  # giving enough space for different translations
        line.submit()
