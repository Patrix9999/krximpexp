from dataclasses import dataclass
from enum import IntEnum
from typing import Any, Dict, List, Tuple

import dearpygui.dearpygui as dpg
from gui_system import JsonHandler
from gui_window_handling import save_window_pos


class AscType(IntEnum):
    MORPH_MESH = 1
    MORPH_ANIM = 2
    STATIC_MESH = 4
    DYNAMIC_MESH = 8
    DYNAMIC_ANIM = 16


class ModelType(IntEnum):
    UNKNOWN = 0
    MORPH = AscType.MORPH_MESH + AscType.MORPH_ANIM
    STATIC = AscType.STATIC_MESH
    DYNAMIC = AscType.DYNAMIC_MESH + AscType.DYNAMIC_ANIM


class ObjectType(IntEnum):
    DUMMY = 0
    BONE = 1
    SLOT = 2
    MESH = 3


class SceneMode(IntEnum):
    REPLACE = 1
    MERGE = 2
    LINK_SLOT_TO_OBJECT = 3
    LINK_BONE_TO_OBJECT = 4
    MERGE_SOFTSKIN_MESH = 5
    REPLACE_SOFTSKIN_MESH = 6
    REPLACE_ANIM = 7
    MERGE_ANIM = 8


@dataclass
class ImportStatus:
    scale: float = 0.01
    scene_mode: int = SceneMode.REPLACE
    selected_slot: list = None
    selected_bone: list = None
    connect_bones: bool = None
    sample_meshes_directory: str = None
    selected_prefix: str = None
    use_sample_dir: bool = None
    start_frame_in_file: int = 0
    end_frame_in_file: int = 100
    start_frame_in_scene: int = 0
    end_frame_in_scene: int = 100
    bone_style: str = "STICK"
    remove_sectored_materials: bool = True
    slot_transparency: bool = False
    color_adjustment: bool = False

    def send(self):
        JsonHandler.save(obj=self, tool="OUTPUT_IMPORT")
        print("SUCCESS")
        save_window_pos()
        dpg.stop_dearpygui()


@dataclass
class ExportStatus:
    scale: float = 100
    selected_objects: list = None
    use_local_cs: bool = True
    matlib_file_path: str = None
    matlib_autorenaming: bool = False

    selected_prefix: str = None
    export_animation: bool = None
    start_frame_in_scene: int = 0
    end_frame_in_scene: int = 100
    start_frame_in_file: int = 0
    end_frame_in_file: int = 100
    german_chars_exception: bool = False
    color_adjustment: bool = False

    def send(self):
        JsonHandler.save(obj=self, tool="OUTPUT_EXPORT")
        print("SUCCESS")
        save_window_pos()
        dpg.stop_dearpygui()


def sort_relations(model_hierarchies: List[Dict[str, Any]]) -> Dict[str, Dict[str, Any]]:
    # Recursively generates a nested dictionary representing the relationships
    # between parent and child objects in the given list of relations
    def get_children(parent: str, relations: List[Tuple[str, str]]) -> Dict[str, Any]:
        # Get the names of all children for the given parent object
        children = (r[1] for r in relations if r[0] == parent)
        # Recursively generate nested dictionaries for each child
        return {c: get_children(c, relations) for c in children}

    # Initialize an empty defaultdict to store the hierarchy relationships for each model prefix
    hierarchy_relations = dict()
    for hierarchy in model_hierarchies:
        # Zip object_parents and objects lists to create a list of (parent, child) tuples
        object_relations = list(zip(hierarchy["object_parents"], hierarchy["objects"]))
        # Convert object_relations to sets of parents and children
        parents, children = map(set, zip(*object_relations))
        # Get the model prefix for this hierarchy
        model_prefix = hierarchy["model_prefix"]
        # Store the hierarchy relationships for this model prefix in the dictionary
        hierarchy_relations[model_prefix] = {
            parent: get_children(parent, object_relations) for parent in (parents - children)
        }
    return hierarchy_relations


def find_children_by_parent_name(
    model_prefix: str, parent_name: str, relations: Dict[str, Dict[str, Any]]
) -> List[str]:
    # Recursively searches the given iterable for a dictionary with the given parent key
    def find_children(iterable: Dict[str, Any], parent: str) -> Any:
        if isinstance(iterable, dict):
            for key, value in iterable.items():
                if key == parent:
                    yield value
                if isinstance(value, dict):
                    yield from find_children(value, parent=parent)

    # Flattens the given nested dictionary into a list of keys (representing child object names)
    def flatten_dict(iterable: Dict[str, Any]) -> str:
        if isinstance(iterable, dict):
            for key, value in iterable.items():
                yield key
                if isinstance(value, dict):
                    yield from flatten_dict(value)

    # Get the hierarchy relationships for the specified model prefix
    model_hierarchy = relations.get(model_prefix)
    if not model_hierarchy:
        # Return None if the model prefix is not found in the relations dictionary
        return None

    # Find the dictionary containing the parent object in the model prefix's hierarchy relationships
    parent_dict = next(find_children(iterable=model_hierarchy, parent=parent_name), None)
    if not parent_dict:
        # Return None if the parent object was not found in the model prefix's hierarchy
        return None

    # Flatten the dictionary into a list of child object names
    return list(flatten_dict(parent_dict))
