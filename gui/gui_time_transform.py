import dearpygui.dearpygui as dpg
import gui_values as val
from gui_window_handling import LayoutHelper, define_size, generic_modal_cfg, render_window_center


def update_input_values(input_setup=None):
    if input_setup is None:
        return

    with dpg.mutex():
        frame_length = (
            dpg.get_value(f"last_{input_setup[0]}_frame_input")
            - dpg.get_value(f"first_{input_setup[0]}_frame_input")
            + 1
        )
        dpg.set_value(
            f"last_{input_setup[1]}_frame_input",
            dpg.get_value(f"first_{input_setup[1]}_frame_input") + frame_length - 1,
        )
        dpg.set_value(f"second_{input_setup[0]}_frame_input", dpg.get_value(f"first_{input_setup[0]}_frame_input") + 1)
        dpg.set_value(f"second_{input_setup[1]}_frame_input", dpg.get_value(f"first_{input_setup[1]}_frame_input") + 1)
        dpg.set_value(f"third_{input_setup[0]}_frame_input", dpg.get_value(f"first_{input_setup[0]}_frame_input") + 2)
        dpg.set_value(f"third_{input_setup[1]}_frame_input", dpg.get_value(f"first_{input_setup[1]}_frame_input") + 2)

        dpg.configure_item("second_frame_timetransform_group", show=(frame_length >= 3))
        dpg.configure_item("third_frame_timetransform_group", show=(frame_length >= 4))
        dpg.configure_item("sep_timetransform_group", show=(frame_length >= 5))


def first_input_file_change_callback(sender, app_data, user_data):
    (input_setup,) = user_data
    if dpg.get_value(f"last_{input_setup[0]}_frame_input") < dpg.get_value(f"first_{input_setup[0]}_frame_input"):
        dpg.set_value(f"last_{input_setup[0]}_frame_input", dpg.get_value(f"first_{input_setup[0]}_frame_input"))
    update_input_values(input_setup=input_setup)


def last_input_file_change_callback(sender, app_data, user_data):
    (input_setup,) = user_data
    if dpg.get_value(f"first_{input_setup[0]}_frame_input") > dpg.get_value(f"last_{input_setup[0]}_frame_input"):
        dpg.set_value(f"first_{input_setup[0]}_frame_input", dpg.get_value(f"last_{input_setup[0]}_frame_input"))
    update_input_values(input_setup=input_setup)


def first_input_scene_change_callback(sender, app_data, user_data):
    (input_setup,) = user_data
    update_input_values(input_setup=input_setup)


def time_apply_button_callback(sender, app_data, user_data):
    (input_setup,) = user_data
    apply_time_button_text = f"{f'{val.time_button_label_noval_text}:'} {dpg.get_value(f'first_{input_setup[0]}_frame_input')} > {dpg.get_value(f'first_{input_setup[1]}_frame_input')}, ..., {dpg.get_value(f'last_{input_setup[0]}_frame_input')} > {dpg.get_value(f'last_{input_setup[1]}_frame_input')}"
    dpg.configure_item("time_transform_button", label=apply_time_button_text)
    dpg.configure_item("time_popup", show=False)


def time_cancel_button_callback(sender, app_data, user_data):
    anim_start_frame, anim_end_frame, input_setup = user_data
    dpg.set_value(f"first_{input_setup[0]}_frame_input", anim_start_frame)
    dpg.set_value(f"first_{input_setup[1]}_frame_input", anim_start_frame)
    dpg.set_value(f"last_{input_setup[0]}_frame_input", anim_end_frame)
    dpg.set_value(f"last_{input_setup[1]}_frame_input", anim_end_frame)
    update_input_values()
    apply_time_button_text = f"{f'{val.time_button_label_noval_text}:'} {dpg.get_value(f'first_{input_setup[0]}_frame_input')} > {dpg.get_value(f'first_{input_setup[1]}_frame_input')}, ..., {dpg.get_value(f'last_{input_setup[0]}_frame_input')} > {dpg.get_value(f'last_{input_setup[1]}_frame_input')}"
    dpg.configure_item("time_transform_button", label=apply_time_button_text)
    dpg.configure_item("time_popup", show=False)


def init_time_popup(data, mode=None):

    anim_start_frame = data.get("anim_start_frame")
    anim_end_frame = data.get("anim_end_frame")

    modal_cfg = generic_modal_cfg
    modal_cfg["width"] = define_size(580)
    modal_cfg["height"] = define_size(300)
    modal_cfg["no_title_bar"] = False

    if mode == "IMPORT":
        input_setup = ["file", "scene"]
        window_label = "Map file keyframes to scene keyframes"
        min_frame = data.get("min_scene_frame")
        max_frame = data.get("max_scene_frame")
    elif mode == "EXPORT":
        input_setup = ["scene", "file"]
        window_label = "Map scene keyframes to file keyframes"
        min_frame = -32768  # min gothic support
        max_frame = 32767  # max gothic support

    with dpg.window(tag="time_popup", label=window_label, **modal_cfg):
        line = LayoutHelper()
        line.add_widget(dpg.add_spacer(), 20)

        if mode == "IMPORT":
            line.add_widget(dpg.add_button(label=val.file_frames_text), 25)
            line.add_widget(dpg.add_spacer(), 3)
            line.add_widget(dpg.add_button(label=val.scene_frames_text), 25)
        elif mode == "EXPORT":
            line.add_widget(dpg.add_button(label=val.scene_frames_text), 25)
            line.add_widget(dpg.add_spacer(), 3)
            line.add_widget(dpg.add_button(label=val.file_frames_text), 25)

        line.submit()
        with dpg.group(tag="first_frame_timetransform_group"):
            line = LayoutHelper()
            line.add_widget(
                dpg.add_button(label=val.first_frame_text, tag="first_frame_text_holder", enabled=False), 20
            )
            line.add_widget(
                dpg.add_input_int(
                    default_value=anim_start_frame,
                    tag=f"first_{input_setup[0]}_frame_input",
                    min_clamped=True,
                    max_clamped=True,
                    min_value=anim_start_frame,
                    max_value=anim_end_frame,
                    callback=first_input_file_change_callback,
                    user_data=(input_setup,),
                ),
                25,
            )
            line.add_widget(dpg.add_button(label=">"), 3)
            line.add_widget(
                dpg.add_input_int(
                    default_value=anim_start_frame,
                    tag=f"first_{input_setup[1]}_frame_input",
                    min_clamped=True,
                    max_clamped=True,
                    min_value=min_frame,
                    max_value=max_frame,
                    callback=first_input_scene_change_callback,
                    user_data=(input_setup,),
                ),
                25,
            )
            line.submit()

        with dpg.group(tag="second_frame_timetransform_group"):
            line = LayoutHelper()
            line.add_widget(dpg.add_button(label=val.second_frame_text, tag="second_frame_text_holder"), 20)
            line.add_widget(
                dpg.add_input_text(default_value=1, tag=f"second_{input_setup[0]}_frame_input", enabled=False), 25
            )
            line.add_widget(dpg.add_button(label=">"), 3)
            line.add_widget(
                dpg.add_input_text(
                    default_value=1,
                    tag=f"second_{input_setup[1]}_frame_input",
                    decimal=True,
                    no_spaces=True,
                    enabled=False,
                ),
                25,
            )
            line.submit()

        with dpg.group(tag="third_frame_timetransform_group"):
            line = LayoutHelper()
            line.add_widget(dpg.add_button(label=val.third_frame_text, tag="third_frame_text_holder"), 20)
            line.add_widget(
                dpg.add_input_text(
                    default_value=1,
                    tag=f"third_{input_setup[0]}_frame_input",
                    decimal=True,
                    no_spaces=True,
                    enabled=False,
                ),
                25,
            )
            line.add_widget(dpg.add_button(label=">"), 3)
            line.add_widget(
                dpg.add_input_text(
                    default_value=1,
                    tag=f"third_{input_setup[1]}_frame_input",
                    decimal=True,
                    no_spaces=True,
                    enabled=False,
                ),
                25,
            )
            line.submit()

        with dpg.group(tag="sep_timetransform_group"):
            dpg.add_button(label=".\n.\n.\n", width=-1)

        line = LayoutHelper()
        line.add_widget(dpg.add_button(label=val.last_frame_text), 20)
        line.add_widget(
            dpg.add_input_int(
                default_value=anim_end_frame,
                tag=f"last_{input_setup[0]}_frame_input",
                min_clamped=True,
                max_clamped=True,
                min_value=anim_start_frame,
                max_value=anim_end_frame,
                callback=last_input_file_change_callback,
                user_data=(input_setup,),
            ),
            25,
        )
        line.add_widget(dpg.add_button(label=">"), 3)
        line.add_widget(
            dpg.add_input_text(
                default_value=1, tag=f"last_{input_setup[1]}_frame_input", decimal=True, no_spaces=True, enabled=False
            ),
            25,
        )
        line.submit()
        dpg.add_separator()
        line = LayoutHelper()
        line.add_widget(dpg.add_spacer(), 10)
        line.add_widget(
            dpg.add_button(label=val.ok_text, callback=time_apply_button_callback, user_data=(input_setup,)), 10
        )
        line.add_widget(
            dpg.add_button(
                label=val.cancel_text,
                callback=time_cancel_button_callback,
                user_data=(anim_start_frame, anim_end_frame, input_setup),
            ),
            10,
        )
        line.add_widget(dpg.add_spacer(), 10)
        line.submit()

        update_input_values(input_setup=input_setup)


def init_time_transform(data, mode=None):
    def callback():
        with dpg.mutex():
            dpg.configure_item("time_popup", show=True)
            render_window_center("time_popup")

    if mode == "IMPORT":
        input_setup = ["file", "scene"]
    elif mode == "EXPORT":
        input_setup = ["scene", "file"]

    init_time_popup(data, mode=mode)
    with dpg.group(horizontal=True):
        line = LayoutHelper()
        line.add_widget(dpg.add_button(label=f"{val.time_space_transformation_text}:", enabled=False), 30)
        # the fstring below assembles placeholder values
        apply_time_button_text = f"{f'{val.time_button_label_noval_text}:'} {dpg.get_value(f'first_{input_setup[0]}_frame_input')} > {dpg.get_value(f'first_{input_setup[1]}_frame_input')}, ..., {dpg.get_value(f'last_{input_setup[0]}_frame_input')} > {dpg.get_value(f'last_{input_setup[1]}_frame_input')}"
        line.add_widget(
            dpg.add_button(label=apply_time_button_text, width=-1, callback=callback, tag="time_transform_button"), 70
        )  # giving enough space for different translations
        line.submit()
