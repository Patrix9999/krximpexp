import dearpygui.dearpygui as dpg
import gui_values as val  # keep gui_ imports to absolute minimum, try offloading to callbacks as in init_confirmation_buttons function
from gui_options import init_options_popup
from gui_scale import get_scale_from_config, init_scale_popup
from gui_system import get_filename_from_path, get_filesize
from gui_time_transform import init_time_popup
from gui_window_handling import LayoutHelper, default_button_size, render_window_center, save_window_pos


def file_picker_callback():  # Invokes native blender file picker wrapper
    print("INVOKE_FILE_PICKER")
    dpg.stop_dearpygui()


def toggle_sample_meshes_callback(sender, app_data, user_data):
    with dpg.mutex():
        dpg.configure_item("sample_meshes_input", show=app_data)
        dpg.configure_item("sample_meshes_button", show=app_data)


def check_existing_prefixes_callback(sender, app_data, user_data):
    (plugin_data,) = user_data
    if plugin_data and plugin_data.get("prefixes", None):
        if app_data in plugin_data["prefixes"]:
            dpg.configure_item("confirm_button", enabled=False)
            if plugin_data.get("multi_scene_mode", False):
                tooltip_value = val.tooltip_cannot_import_multiscene_prefix_exists
            else:
                tooltip_value = val.tooltip_cannot_import_prefix_exists

            if not dpg.does_item_exist("tooltip_prefix_exists"):
                dpg.add_text(tooltip_value, parent="prefix_group", tag="tooltip_prefix_exists")
                dpg.bind_item_theme("tooltip_prefix_exists", "warning_red_theme")
        else:
            dpg.configure_item("confirm_button", enabled=True)
            dpg.delete_item("tooltip_prefix_exists")


def handle_unique_prefix_button_callback(sender, app_data, user_data):
    (plugin_data,) = user_data
    if plugin_data and plugin_data.get("unique_prefix"):
        dpg.set_value("scene_unique_prefix_input", plugin_data["unique_prefix"])
    check_existing_prefixes_callback(sender, app_data, user_data)


def handle_unique_prefix_callback(sender, app_data, user_data):
    (plugin_data,) = user_data
    if plugin_data and plugin_data.get("unique_prefix"):
        if dpg.get_value("scene_radio_button") == val.scene_choice[0]:
            if plugin_data.get("multi_scene_mode", False):
                dpg.set_value("scene_unique_prefix_input", plugin_data["unique_prefix"])
            else:
                dpg.set_value("scene_unique_prefix_input", "")
            dpg.configure_item("unique_prefix_button", enabled=False)
            dpg.configure_item("scene_unique_prefix_input", enabled=False)
        else:
            dpg.set_value("scene_unique_prefix_input", plugin_data["unique_prefix"])
            dpg.configure_item("unique_prefix_button", enabled=True)
            dpg.configure_item("scene_unique_prefix_input", enabled=True)
    check_existing_prefixes_callback(sender, app_data, user_data)


def handle_slots_bones_handling_callback(sender, app_data, user_data):
    (plugin_data,) = user_data
    handle_unique_prefix_callback(sender, app_data, user_data)
    if plugin_data:
        slots = plugin_data.get("slots", None)
        selected_slots = plugin_data.get("selected_slots", None)
        bones = plugin_data.get("bones", None)
        selected_bones = plugin_data.get("selected_bones", None)
        scene_mode = dpg.get_value("scene_radio_button")
        if (scene_mode == val.scene_choice_3ds_import[2] or scene_mode == val.scene_choice_morphmesh[2]) and slots:
            if not selected_slots or len(selected_slots) == 0:
                dpg.configure_item("scene_todo_combo_button", show=True, items=slots, default_value=slots[0])
            else:
                dpg.configure_item("scene_todo_combo_button", show=True, items=slots, default_value=selected_slots[0])
            dpg.configure_item("scene_todo_slot_bone_text", show=True)
            dpg.set_value("scene_todo_slot_bone_text", val.choose_slot_text)
        elif (scene_mode == val.scene_choice_3ds_import[3] or scene_mode == val.scene_choice_morphmesh[3]) and bones:
            if not selected_bones or len(selected_bones) == 0:
                dpg.configure_item("scene_todo_combo_button", show=True, items=bones, default_value=bones[0])
            else:
                dpg.configure_item("scene_todo_combo_button", show=True, items=bones, default_value=selected_bones[0])
            dpg.configure_item("scene_todo_slot_bone_text", show=True)
            dpg.set_value("scene_todo_slot_bone_text", val.choose_bone_text)
        else:
            dpg.configure_item("scene_todo_combo_button", show=False)
            dpg.configure_item("scene_todo_slot_bone_text", show=False)


def init_file_properties(file_name, mode="IMPORT"):
    def call_toggle_full_path(sender, app_data, user_data):
        # https://github.com/hoffstadt/DearPyGui/discussions/1596
        with dpg.mutex():
            (toggle,) = user_data
            if toggle:
                dpg.configure_item("file_name_text", default_value=file_name)
                dpg.configure_item("file_name_button", label=f"{val.file_path_text}:")
            else:
                dpg.configure_item("file_name_text", default_value=get_filename_from_path(file_name))
                dpg.configure_item("file_name_button", label=f"{val.file_name_text}:")
            toggle = not toggle

            dpg.set_item_user_data(sender, (toggle,))

    with dpg.group():
        if mode == "IMPORT":
            dpg.add_text(val.file_properties_text)

        with dpg.group(horizontal=True, tag="file_properties"):
            dpg.add_button(
                tag="file_name_button",
                width=default_button_size["w"],
                label=f"{val.file_name_text}:",
                callback=call_toggle_full_path,
                user_data=(True,),
            )
            dpg.add_input_text(
                tag="file_name_text", width=-1, default_value=get_filename_from_path(file_name), enabled=False
            )

            with dpg.tooltip("file_name_text"):
                dpg.add_text(val.tooltip_path_name)

            with dpg.tooltip("file_name_button"):
                dpg.add_text(val.tooltip_path_name)

        if mode != "EXPORT":
            with dpg.group(horizontal=True, tag="file_size_properties"):
                dpg.add_button(label=f"{val.file_size_text}:", width=default_button_size["w"])
                dpg.add_input_text(default_value=get_filesize(file_name), width=-1, enabled=False)


def asc_model_stats(plugin_data=None):
    if plugin_data:
        dpg.add_text(val.model_info_text)
        line = LayoutHelper()
        line.add_widget(dpg.add_button(label=f"{val.meshes_text}: {plugin_data['num_file_meshes']}", enabled=False), 33)
        line.add_widget(dpg.add_button(label=f"{val.slots_text}: {plugin_data['num_file_slots']}", enabled=False), 33)
        line.add_widget(dpg.add_button(label=f"{val.bones_text}: {plugin_data['num_file_bones']}", enabled=False), 33)
        line.submit()


def asc_animation_stats(plugin_data=None):
    if plugin_data:
        dpg.add_text(val.anim_info_text)
        line = LayoutHelper()
        line.add_widget(
            dpg.add_button(label=f"{val.first_frame_text}: {plugin_data['anim_start_frame']}", enabled=False), 33
        )
        line.add_widget(
            dpg.add_button(label=f"{val.last_frame_text}: {plugin_data['anim_end_frame']}", enabled=False), 33
        )
        line.add_widget(
            dpg.add_button(label=f"{val.frame_rate_text}: {plugin_data['anim_frame_rate']}", enabled=False), 33
        )
        line.submit()


def close_button():
    def callback():
        save_window_pos()
        dpg.stop_dearpygui()

    return dpg.add_button(label=val.cancel_text, callback=callback, tag="close_button")


def options_button():
    def callback():
        with dpg.mutex():
            dpg.configure_item("options_popup", show=True)
            render_window_center("options_popup")

    init_options_popup()
    return dpg.add_button(label=val.options_text, callback=callback, tag="options_button")


def init_time_and_scale_transform(data, mode=None, parent=None):
    def scale_callback():
        with dpg.mutex():
            dpg.configure_item("scale_popup", show=True)
            render_window_center("scale_popup")

    def time_callback():
        with dpg.mutex():
            dpg.configure_item("time_popup", show=True)
            render_window_center("time_popup")

    if parent == None:
        parent = "WINDOW_PANEL"

    if mode == "IMPORT":
        input_setup = ["file", "scene"]
    elif mode == "EXPORT":
        input_setup = ["scene", "file"]

    init_scale_popup(mode)
    init_time_popup(data, mode)
    with dpg.group(parent=parent):
        line = LayoutHelper()
        line.add_widget(
            dpg.add_button(
                label=f"{val.scale_button_label_noval_text}: {get_scale_from_config(mode)}",
                width=-1,
                callback=scale_callback,
                tag="scale_button",
            ),
            30,
        )
        apply_time_button_text = f"{f'{val.time_button_label_noval_text}:'} {dpg.get_value(f'first_{input_setup[0]}_frame_input')} > {dpg.get_value(f'first_{input_setup[1]}_frame_input')}, ..., {dpg.get_value(f'last_{input_setup[0]}_frame_input')} > {dpg.get_value(f'last_{input_setup[1]}_frame_input')}"
        line.add_widget(
            dpg.add_button(label=apply_time_button_text, width=-1, callback=time_callback, tag="time_transform_button"),
            70,
        )
        line.submit()


def init_confirmation_buttons(callback: callable, mode="IMPORT"):
    if mode == "IMPORT":
        mode = val.import_text
    else:
        mode = val.export_text

    line = LayoutHelper()
    line.add_widget(dpg.add_spacer(), 25)
    line.add_widget(dpg.add_button(label=mode, tag="confirm_button", callback=callback), 33)
    line.add_widget(options_button(), 33)
    line.add_widget(close_button(), 33)
    line.add_widget(dpg.add_spacer(), 25)
    line.submit()
