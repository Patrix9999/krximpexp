import gui_values as val
from gui_system import JsonHandler, config

try:
    import dearpygui.dearpygui as dpg
except ImportError as e:  # TODO: Selfhealing
    raise Exception(e)

from gui_generics import (
    asc_model_stats,
    check_existing_prefixes_callback,
    handle_slots_bones_handling_callback,
    handle_unique_prefix_button_callback,
    handle_unique_prefix_callback,
    init_confirmation_buttons,
    init_file_properties,
)
from gui_scale import init_scale_button
from gui_window_handling import LayoutHelper, WindowProperties, define_size, initialize_context, window_handling
from message_box import gui_messagebox

from helpers import ImportStatus, SceneMode


def krx_import(title=val.window_title_import_asc_morphmesh):
    def import_callback():
        status = ImportStatus()
        status.color_adjustment = dpg.get_value("color_adjustment_slider")

        scene_mode = dpg.get_value("scene_radio_button")

        if scene_mode == val.scene_choice[0]:
            status.scene_mode = SceneMode.REPLACE
        elif scene_mode == val.scene_choice[1]:
            status.scene_mode = SceneMode.MERGE
        elif scene_mode == val.scene_choice_morphmesh[2]:
            status.selected_slot = dpg.get_value("scene_todo_combo_button")
            status.scene_mode = SceneMode.LINK_SLOT_TO_OBJECT
        elif scene_mode == val.scene_choice_morphmesh[3]:
            status.selected_bone = dpg.get_value("scene_todo_combo_button")
            status.scene_mode = SceneMode.LINK_BONE_TO_OBJECT

        status.selected_prefix = dpg.get_value("scene_unique_prefix_input")
        status.scale = float(dpg.get_value("scale_input"))

        config.handle["SCENE"]["import_mode"] = int(status.scene_mode)

        status.send()

    tool_mode = "IMPORT"
    scene_mode = "SIMPLE"
    plugin_data = JsonHandler.load(tool=f"INPUT_{tool_mode}")
    slots = plugin_data.get("slots", None)
    bones = plugin_data.get("bones", None)
    file_name = plugin_data["file_path"]
    win_width = 600
    win_height = 520

    if plugin_data["appropriate_prefixes"]:
        for i, prefix in enumerate(plugin_data["appropriate_prefixes"]):
            if prefix == val.empty_text:
                gui_messagebox(file_name=file_name, error_message=f"{val.error_messages[5]} {prefix}")
                exit()
            if prefix == "":
                if plugin_data["selected_prefix"] == prefix:
                    plugin_data["selected_prefix"] = val.empty_text
                plugin_data["appropriate_prefixes"][i] = val.empty_text


    # Need to make shallow copy of the original values since the originals are used as a reference for callbacks
    scene_choice_morphmesh = val.scene_choice_morphmesh.copy()

    if bones and slots:
        scene_mode = "EXTENDED"
        win_height += 60

    if slots and scene_mode == "SIMPLE":
        scene_mode = "EXTENDED"
        scene_choice_morphmesh.pop(3)
        win_height += 40
    if bones and scene_mode == "SIMPLE":
        scene_mode = "EXTENDED"
        scene_choice_morphmesh.pop(2)
        win_height += 40



    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(win_width), "h": define_size(win_height)}

    initialize_context()

    # main window
    with dpg.window(tag="WINDOW_PANEL"):
        init_file_properties(file_name)

        dpg.add_separator()
        asc_model_stats(plugin_data)

        dpg.add_separator()
        if scene_mode == "SIMPLE":
            with dpg.group():
                dpg.add_text(val.scene_todo_text)
                dpg.add_radio_button(
                    items=val.scene_choice,
                    default_value=val.scene_choice[0],
                    callback=handle_unique_prefix_callback,
                    user_data=(plugin_data,),
                    tag="scene_radio_button",
                )
        else:
            with dpg.group():
                dpg.add_text(val.scene_todo_text)
                dpg.add_radio_button(
                    items=scene_choice_morphmesh,
                    default_value=scene_choice_morphmesh[0],
                    callback=handle_slots_bones_handling_callback,
                    user_data=(plugin_data,),
                    tag="scene_radio_button",
                )
                with dpg.group(horizontal=True):
                    dpg.add_text(show=False, tag="scene_todo_slot_bone_text")
                    dpg.add_combo(width=-1, show=False, tag="scene_todo_combo_button")

        dpg.add_separator()
        dpg.add_text(val.model_prefix_text)
        dpg.add_text(val.model_prefix_desc, wrap=window_handler.dimension["w"] - define_size(50))
        with dpg.group(tag="prefix_group"):
            line = LayoutHelper()
            line.add_widget(
                dpg.add_input_text(
                    tag="scene_unique_prefix_input",
                    default_value=plugin_data["unique_prefix"],
                    callback=check_existing_prefixes_callback,
                    user_data=(plugin_data,),
                ),
                75,
            )
            line.add_widget(
                dpg.add_button(
                    tag="unique_prefix_button",
                    label=val.auto_text,
                    callback=handle_unique_prefix_button_callback,
                    user_data=(plugin_data,),
                ),
                25,
            )
            line.submit()

        dpg.add_separator()
        scene_radio = dpg.get_value("scene_radio_button")
        if scene_radio == val.scene_choice[0]:
            if plugin_data["multi_scene_mode"]:
                dpg.set_value("scene_unique_prefix_input", plugin_data["unique_prefix"])
            else:
                dpg.set_value("scene_unique_prefix_input", "")
            dpg.configure_item("unique_prefix_button", enabled=False)
            dpg.configure_item("scene_unique_prefix_input", enabled=False)
        init_scale_button(mode=tool_mode)

        dpg.add_separator()
        init_confirmation_buttons(import_callback, mode=tool_mode)

    window_handling(window_handler, title)


if __name__ == "__main__":
    krx_import()
