import gui_values as val
from gui_system import JsonHandler, config

try:
    import dearpygui.dearpygui as dpg
except (ModuleNotFoundError, ImportError) as e:  # TODO: Selfhealing
    raise Exception(e)

from gui_generics import (
    asc_model_stats,
    check_existing_prefixes_callback,
    file_picker_callback,
    handle_unique_prefix_button_callback,
    handle_unique_prefix_callback,
    init_confirmation_buttons,
    init_file_properties,
    toggle_sample_meshes_callback,
)
from gui_scale import init_scale_button
from gui_window_handling import LayoutHelper, WindowProperties, define_size, initialize_context, window_handling, get_id
from message_box import gui_messagebox

from helpers import ImportStatus, SceneMode


def krx_import(title=val.window_title_import_asc_model):
    def import_callback():
        status = ImportStatus()
        status.color_adjustment = dpg.get_value("color_adjustment_slider")

        scene_tab = dpg.get_value("asc_bar")

        # DPG v1.9.1 issue changed this to int
        if isinstance(scene_tab, str):
            scene_tab = get_id(scene_tab)

        if scene_tab == get_id("asc_tab_new_model"):
            scene_mode = dpg.get_value("scene_radio_button")
            if scene_mode == val.scene_choice[0]:
                status.scene_mode = SceneMode.REPLACE  # reset scene
            elif scene_mode == val.scene_choice[1]:
                status.scene_mode = SceneMode.MERGE

            status.selected_prefix = dpg.get_value("scene_unique_prefix_input")
            status.connect_bones = dpg.get_value("connect_bones")
            status.slot_transparency = 0.01 * dpg.get_value("slot_transparency")
            status.use_sample_dir = dpg.get_value("sample_meshes_checkbox")
            status.sample_meshes_directory = dpg.get_value("sample_meshes_input")

            config.handle["SCENE"]["import_mode"] = int(status.scene_mode)
            config.handle["SCENE"]["slot_transparency"] = dpg.get_value("slot_transparency")
            config.handle["SCENE"]["connect_bones"] = status.connect_bones
            config.handle["SCENE"]["sample_meshes_directory"] = status.sample_meshes_directory

        elif scene_tab == get_id("asc_tab_merge_model"):
            scene_mode = dpg.get_value("scene_radio_asc_button")
            if scene_mode == val.scene_choice_replace_merge[0]:
                status.scene_mode = SceneMode.MERGE_SOFTSKIN_MESH  # replace softskin to current armature
            elif scene_mode == val.scene_choice_replace_merge[1]:
                status.scene_mode = SceneMode.REPLACE_SOFTSKIN_MESH  # merge softskin to current armature
            status.selected_prefix = dpg.get_value("scene_prefix_merge_replace_button")
            if status.selected_prefix == val.empty_text:
                status.selected_prefix = ""

        else:
            status.scene_mode = SceneMode.MERGE

        config.handle["SCENE"]["bone_style"] = dpg.get_value("bone_styles_combo")
        status.bone_style = config.handle["SCENE"]["bone_style"].upper()

        status.scale = float(dpg.get_value("scale_input"))
        status.send()

    tool_mode = "IMPORT"
    plugin_data = JsonHandler.load(tool=f"INPUT_{tool_mode}")
    file_name = plugin_data["file_path"]

    if plugin_data["appropriate_prefixes"]:
        for i, prefix in enumerate(plugin_data["appropriate_prefixes"]):
            if prefix == val.empty_text:
                gui_messagebox(file_name=file_name, error_message=f"{val.error_messages[5]} {prefix}")
                exit()
            if prefix == "":
                if plugin_data["selected_prefix"] == prefix:
                    plugin_data["selected_prefix"] = val.empty_text
                plugin_data["appropriate_prefixes"][i] = val.empty_text

    window_handler = WindowProperties()
    window_handler.dimension = {"w": define_size(600), "h": define_size(780)}

    initialize_context()

    # main window
    with dpg.window(tag="WINDOW_PANEL"):
        init_file_properties(file_name)
        dpg.add_separator()

        asc_model_stats(plugin_data)
        dpg.add_separator()

        with dpg.tab_bar(tag="asc_bar"):
            with dpg.tab(label=val.create_a_model_text, tag="asc_tab_new_model"):
                with dpg.group():
                    dpg.add_text(val.scene_todo_text)
                    dpg.add_radio_button(
                        items=val.scene_choice,
                        default_value=val.scene_choice[0],
                        tag="scene_radio_button",
                        callback=handle_unique_prefix_callback,
                        user_data=(plugin_data,),
                    )

                dpg.add_separator()

                dpg.add_text(val.model_prefix_text)
                dpg.add_text(val.model_prefix_desc, wrap=window_handler.dimension["w"] - define_size(50))
                with dpg.group(tag="prefix_group"):
                    line = LayoutHelper()
                    line.add_widget(
                        dpg.add_input_text(
                            tag="scene_unique_prefix_input",
                            default_value=plugin_data["unique_prefix"],
                            callback=check_existing_prefixes_callback,
                            user_data=(plugin_data,),
                        ),
                        75,
                    )
                    line.add_widget(
                        dpg.add_button(
                            tag="unique_prefix_button",
                            label=val.auto_text,
                            callback=handle_unique_prefix_button_callback,
                            user_data=(plugin_data,),
                        ),
                        25,
                    )
                    line.submit()

                dpg.add_separator()
                if dpg.get_value("scene_radio_button") == val.scene_choice[0]:
                    if plugin_data["multi_scene_mode"]:
                        dpg.set_value("scene_unique_prefix_input", plugin_data["unique_prefix"])
                    else:
                        dpg.set_value("scene_unique_prefix_input", "")
                    dpg.configure_item("unique_prefix_button", enabled=False)
                    dpg.configure_item("scene_unique_prefix_input", enabled=False)
                dpg.add_text(val.bones_and_slots_text)
                with dpg.group(horizontal=True):
                    dpg.add_text(val.slot_transparency_text)
                    dpg.add_slider_int(
                        tag="slot_transparency",
                        default_value=config.handle["SCENE"]["slot_transparency"],
                        max_value=100,
                        min_value=0,
                        format="%i%%",
                        width=-1,
                    )
                dpg.add_checkbox(
                    label=val.connect_bones_text,
                    tag="connect_bones",
                    default_value=config.handle["SCENE"]["connect_bones"],
                )
                dpg.add_checkbox(
                    label=f"{val.use_sample_meshes_text}",
                    tag="sample_meshes_checkbox",
                    default_value=True,
                    callback=toggle_sample_meshes_callback,
                )
                line = LayoutHelper()
                if plugin_data["sample_meshes_directory"]:
                    sample_meshes_path = plugin_data["sample_meshes_directory"]
                else:
                    sample_meshes_path = config.handle["SCENE"]["sample_meshes_directory"]
                line.add_widget(dpg.add_input_text(default_value=sample_meshes_path, tag="sample_meshes_input"), 75)
                line.add_widget(
                    dpg.add_button(label="...", tag="sample_meshes_button", callback=file_picker_callback), 25
                )
                line.submit()
                dpg.add_separator()
                dpg.add_text(val.bone_style_text)
                dpg.add_combo(
                    val.bone_styles,
                    default_value=config.handle["SCENE"]["bone_style"],
                    width=-1,
                    tag="bone_styles_combo",
                )
                dpg.add_separator()
            if plugin_data["appropriate_prefixes"]:
                with dpg.tab(label=val.merge_with_model_text, tag="asc_tab_merge_model"):
                    with dpg.group():
                        dpg.add_text(val.scene_todo_existing_model_text)
                        dpg.add_radio_button(
                            items=val.scene_choice_replace_merge,
                            default_value=val.scene_choice_replace_merge[0],
                            tag="scene_radio_asc_button",
                        )

                    dpg.add_separator()

                    dpg.add_text(val.model_prefix_text)
                    dpg.add_text(val.model_prefix_merging_desc, wrap=window_handler.dimension["w"] - define_size(50))
                    dpg.add_combo(
                        items=plugin_data["appropriate_prefixes"],
                        default_value=plugin_data["selected_prefix"],
                        width=-1,
                        tag="scene_prefix_merge_replace_button",
                    )
                    dpg.add_separator()

        init_scale_button(mode=tool_mode)  # button + popup window
        dpg.add_separator()

        init_confirmation_buttons(import_callback, mode=tool_mode)

    window_handling(window_handler, title)


if __name__ == "__main__":
    krx_import()
