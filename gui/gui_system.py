import gettext
import os
import sys
from pathlib import Path
from typing import Dict

GUI_ROOT: Path = Path(os.path.dirname(os.path.realpath(__file__)))
"""Path to the root directory of the GUI submodule"""

PLUGIN_ROOT: Path = GUI_ROOT.parent
"""Path to the root directory of the plugin"""

if str(PLUGIN_ROOT) not in sys.path:
    sys.path.insert(0, str(PLUGIN_ROOT))

# Hack to reuse the same declarations as the plugin root
# noinspection PyUnresolvedReferences
import system as plugin_system

sys.path.remove(str(PLUGIN_ROOT))

MACHINE: str = plugin_system.MACHINE
"""Machine/CPU architecture"""

PYTHON_VERSION: str = plugin_system.PYTHON_VERSION
"""Version of the running Python executable"""

SYSTEM: str = plugin_system.SYSTEM
"""System variant"""

PLUGIN_SITE_PACKAGES: str = plugin_system.PLUGIN_SITE_PACKAGES
"""String path to the site-packages directory of the plugin. Used by pip to install packages."""

LOCALE_PATH = GUI_ROOT / "locale"
"""Path to the root directory of the GUI locale file"""

if PLUGIN_SITE_PACKAGES not in sys.path:
    sys.path.insert(0, PLUGIN_SITE_PACKAGES)

if SYSTEM == "LINUX":
    os.environ["DISPLAY"] = ":0"  # https://gitlab.com/Patrix9999/krximpexp/-/issues/22#note_1499740408 Let's force that from here.
    os.environ["__GLVND_DISALLOW_PATCHING"] = "1"  # https://github.com/hoffstadt/DearPyGui/issues/554#issuecomment-1852406840

JsonHandler = plugin_system.JsonHandler


def get_filename_from_path(_path):
    return os.path.basename(os.path.realpath(_path))


def get_filesize(file_path, suffix="B") -> str:
    num = os.path.getsize(file_path)
    for unit in ["", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"]:
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


class SettingsManager:
    def __init__(self):
        self.default_config: Dict = {
            "OPTIONS": {"language": "en_US", "dpi_multiplier": 1.0, "first_run": True, "window_pos_x": None, "window_pos_y": None},
            "SCENE": {
                "import_mode": 0,
                "bone_style": "Stick",
                "remove_sectored_materials": True,
                "slot_transparency": 100,
                "connect_bones": True,
                "use_sample_meshes": True,
                "sample_meshes_directory": f"{PLUGIN_ROOT / 'sample_meshes'}",
                "color_adjustment": 0,
            },
            "SCALE": {"setup_unit": 5, "system_units_per_file_unit": 0.01, "file_units_per_system_unit": 100.0},
            "MATLIB": {"autorenaming": False, "matlibini_path": None},
        }
        self.settings_path: Path = GUI_ROOT / "settings"
        self.handle = self.load()

    def _sync_settings_with_config(self, settings: Dict, default_config: Dict):
        for key, value in list(settings.items()):
            if key not in default_config:
                del settings[key]
            elif isinstance(value, dict):
                self._sync_settings_with_config(settings[key], default_config[key])
            elif not isinstance(value, type(default_config[key])) and default_config[key] is not None:
                settings[key] = default_config[key]

        for key, value in list(default_config.items()):
            if key not in settings:
                settings[key] = default_config[key]

    def save(self) -> None:
        JsonHandler.save(self.handle, "settings", dir_path=self.settings_path)

    def load(self) -> Dict:
        settings = JsonHandler.load("settings", dir_path=self.settings_path)

        if settings:
            self._sync_settings_with_config(settings, self.default_config)
            return settings
        else:
            self.handle = self.default_config
            self.save()
            return self.handle


config = SettingsManager()


class LanguageManager:
    def __init__(self):
        self.language_codes = list()
        self.language_texts = list()

        # Populate language_codes based on <language code>/LC_MESSAGES/<Full Language Name>.mo scheme.
        for locale_path in LOCALE_PATH.rglob("*.mo"):
            assembled_locale = str(locale_path).replace(str(LOCALE_PATH), "").split("LC_MESSAGES")
            lang_code = assembled_locale[0].replace("\\", "").replace("/", "")
            full_lang_name = assembled_locale[1].replace("\\", "").replace("/", "").replace(".mo", "")
            self.language_codes.append(lang_code)
            self.language_texts.append(full_lang_name)

        self.language_codes_to_texts = dict(zip(self.language_codes, self.language_texts))
        self.language_texts_to_codes = dict(zip(self.language_texts, self.language_codes))

        full_lang_name = "English"

        if config.handle["OPTIONS"]["language"] not in self.language_codes:
            config.handle["OPTIONS"]["language"] = "en_US"

        code_from_settings = config.handle["OPTIONS"]["language"]

        for code, fullname in self.language_codes_to_texts.items():
            if code == code_from_settings:
                full_lang_name = fullname
                break

        self.gettext_handle = gettext.translation(full_lang_name, localedir=LOCALE_PATH, languages=[code_from_settings])
        self.gettext_handle.install()
