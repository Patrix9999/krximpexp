## 2.0.0 (Unreleased)

Please use this version going forward. This version fixes many bugs. Therefore,
we're considering it incompatible with previously exported files, as they 
likely will load, but the damage has already been done on previous versions.

Not final and inaccurate changelog:

Developer goodies:
- UI was ported to DearPyGUI, which unifies everything into Python, this makes
  modification to the UI easier and faster.
- A lot of the code base was cleaned up and rewritten using Blender built-ins.
- New code modules should fully support batch import/export, from inside 
  scripts, which wasn't the case before.

User experience goodies:
- New UI allows for support for Linux/macOS without any compatibility layers.
- Many times faster performance when loading assets.
- Materials have proper transparency if you use textures with alpha channel.
- Better material and texture handling (import/export encoding support etc.).  
- Better scene management, you can now use multiple scenes with plugin, also 
  cleaning the scene was improved.
- Merging meshes with dynamic models wasn't working like at all, now it probably
  does, so please provide feedback.
- Better and prettier Bmesh triangulation method (which is faster than operator,
  but slower than older one, visual example: https://cdn.discordapp.com/attachments/990576276387532810/1063873394191384768/image.png) 
  (this was mainly done because of this 
  https://discord.com/channels/989316194148433950/990576276387532810/1075148142208622664)  
- 3DS welding will be removed (this is always destructive, blender won't see 
  coplanar faces = it'll try to merge everything together)    
- "Replacing bones" with dynamic skeletal models was removed as that was not
  working at all and it's impossible to do in blender, this was replaced with 
  linking mesh to the bone.

---

## 1.1.0

**[FIXED]** Changing import scale in dialog menu will now work with ZS_ objects.  
**[FIXED]** 3DS export bug.  
**[FIXED]** Exporting ASC from .blend file.  
**[FIXED]** Importing default ASC models, bones won't be messed up.  
**[FIXED]** Parenting bone will now have assigned parent in static models.  
**[FIXED]** Duplicate materials & textures will now be discarded.  
**[UPDATED]** Imported models specular is now set to 0.0f, to avoid shiny effect.  
**[UPDATED]** Exported ASC files will now store only floats with 6 digits precision point, just like original models from Piranha Bytes does.  
**[UPDATED]** Duplicate bone objects will be discarded and instead of error, warning will be displayed in blender console.  
**[UPDATED]** All imported models scale will now be applied, to be in range `<1.0, 1.0, 1.0>`.  
**[UPDATED]** All html help files are now saved as utf-8, removed old html 4 tags.  
**[UPDATED]** Optimized material/uv loading with bmesh (faster loading time).  
**[UPDATED]** Optimized singleton comparison, to use `is` and `is not` instead of `==` and `!=` operators (faster loading time).  
**[ADDED]** Polish translation.

---

## 1.0.0

Initial release